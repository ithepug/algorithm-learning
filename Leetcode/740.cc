#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int deleteAndEarn(vector<int> &nums)
    {
        int themax = *max_element(nums.begin(), nums.end());
        int themin = *min_element(nums.begin(), nums.end());
        int n = themax - themin + 1;
        vector<int> v(n, 0);
        for (auto x : nums)
            v[x - themin] += x;
        vector<int> dp(n);
        dp[0] = v[0];
        if (n >= 2)
            dp[1] = max(v[0], v[1]);
        if (n >= 3)
        {
            for (int i = 2; i != n; i++)
            {
                dp[i] = max(v[i] + dp[i - 2], dp[i - 1]);
            }
        }
        return dp[n - 1];
    }
};