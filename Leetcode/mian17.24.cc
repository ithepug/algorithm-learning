#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<int> getMaxMatrix(vector<vector<int>> &matrix)
    {
        int n = matrix.size();
        if (n < 1)
            return vector<int>();
        vector<int> ret(4);
        int m = matrix[0].size();
        int maxsum = INT_MIN;
        for (int j = 0; j != n; j++) // start
        {
            vector<int> v(m, 0);
            for (int i = 1; j + i <= n; i++) // len
            {
                for (int k = 0; k != m; k++)
                    v[k] += matrix[j + i - 1][k];
                int themax = v[0];
                int themax_l = 0;
                int themax_r = 0;
                int pre = v[0];
                int pre_i = 0;
                for (int k = 1; k != m; k++)
                {
                    if (pre > 0)
                    {
                        pre = v[k] + pre;
                    }
                    else
                    {
                        pre = v[k];
                        pre_i = k;
                    }
                    if (pre > themax)
                    {
                        themax_l = pre_i;
                        themax_r = k;
                        themax = pre;
                    }
                }
                if (themax > maxsum)
                {
                    ret[0] = j;
                    ret[1] = themax_l;
                    ret[2] = j + i - 1;
                    ret[3] = themax_r;
                    maxsum = themax;
                }
            }
        }
        return ret;
    }
};

int main()
{
    Solution s;
    vector<vector<int>> vv;
    vv.push_back({9, -8, 1, 3, -2});
    vv.push_back({-3, 7, 6, -2, 4});
    vv.push_back({6, -4, -4, 8, -7});
    s.getMaxMatrix(vv);
}