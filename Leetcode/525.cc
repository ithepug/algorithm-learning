#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
public:
    int findMaxLength(vector<int> &nums)
    {
        int n = nums.size();
        int preSum = 0;
        unordered_map<int, int> index;
        index[0] = -1;
        int ans = 0;
        for (int i = 0; i != n; i++)
        {
            preSum = preSum + (nums[i] == 0 ? -1 : 1);
            if (index.count(preSum))
                ans = max(ans, i - index[preSum]);
            else
                index[preSum] = i;
        }
        return ans;
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, 0};
    s.findMaxLength(v);
}