#include <iostream>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
public:
    ListNode *partition(ListNode *head, int x)
    {
        ListNode *sh = NULL;
        ListNode *st = NULL;
        ListNode real_head(0);
        real_head.next = head;
        ListNode *node = &real_head;
        if (node->next == NULL)
            return head;
        while (node->next != NULL)
        {
            if (node->next->val < x)
            {
                if (sh == NULL)
                {
                    sh = node->next;
                    st = node->next;
                }
                else
                {
                    st->next = node->next;
                    st = st->next;
                }
                node->next = node->next->next;
                st->next = NULL;
            }
            else
            {
                node = node->next;
            }
        }
        if (st != NULL)
        {
            st->next = real_head.next;
            return sh;
        }
        else
        {
            return head;
        }
    }
};

int main()
{
    ListNode n1(1);
    ListNode n2(4);
    ListNode n3(3);
    ListNode n4(2);
    ListNode n5(5);
    ListNode n6(2);

    n1.next = &n2;
    n2.next = &n3;
    n3.next = &n4;
    n4.next = &n5;
    n5.next = &n6;

    Solution s;
    s.partition(&n1, 3);
}