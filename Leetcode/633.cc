#include <cmath>
#include <iostream>
using namespace std;
class Solution
{
public:
    bool judgeSquareSum(int c)
    {
        int l = 0;
        int r = sqrt(c);
        while (l <= r)
        {
            long long sum = (long long)l * l + r * r;
            if (sum == c)
                return true;
            else if (sum > c)
            {
                r--;
            }
            else if (sum < c)
            {
                l++;
            }
        }
        return false;
    }
};