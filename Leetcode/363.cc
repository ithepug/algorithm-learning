#include <iostream>
#include <set>
#include <vector>
using namespace std;

class Solution
{
public:
    int maxSumSubmatrix(vector<vector<int>> &matrix, int k)
    {
        int ans = INT_MIN;
        int n = matrix.size();
        int m = matrix[0].size();
        for (int i = 0; i != n; i++) // start
        {
            vector<int> v(m, 0);
            for (int j = 0; i + j < n; j++) // len
            {
                for (int k = 0; k != m; k++)
                    v[k] += matrix[i + j][k];

                set<int> sumSet;
                int s = 0;
                sumSet.insert(s);
                for (auto x : v)
                {
                    s += x;
                    auto l = sumSet.lower_bound(s - k);
                    if (l != sumSet.end())
                    {
                        ans = max(ans, s - *l);
                    }
                    sumSet.insert(s);
                }
            }
        }
        return ans;
    }
};

int main()
{
    set<int> ss{1, 2, 3, 5, 6};
    cout << *ss.lower_bound(4) << endl;
    cout << *ss.upper_bound(4) << endl;
    vector<vector<int>> mm;
    mm.push_back({1, 0, 1});
    mm.push_back({0, -2, 3});
    Solution s;
    s.maxSumSubmatrix(mm, 2);
}