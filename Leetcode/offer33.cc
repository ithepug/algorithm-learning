#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    bool verifyPostorder(vector<int> &postorder)
    {
        return check(postorder, 0, postorder.size());
    }

    bool check(const vector<int> &order, const int &l, const int &r)
    {
        if (l + 1 >= r)
            return true;
        int p = l;
        int root = order[r - 1];
        while (p < order.size() && order[p] < root) // 左子树每个元素都大于根节点
        {
            p++;
        }
        int m = p;
        while (p < order.size() && order[p] > root) // 右子树每个元素都大于根节点
        {
            p++;
        }
        return p == r - 1 && check(order, l, m) && check(order, m, r - 1);
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, 3, 2, 6, 5};
    s.verifyPostorder(v);
}