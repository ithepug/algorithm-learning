#include <iostream>
#include <vector>

using namespace std;
class Solution
{
private:
    string s1;
    string s2;

public:
    string SearchPath(const vector<vector<int>> &dir, int i, int j)
    {
        if (i == 0 && j == 0)
            return "";
        if (dir[i][j] == 0)
        {
            if (s1[i - 1] == s2[j - 1])
            {
                return SearchPath(dir, i - 1, j - 1) + s1[i - 1];
            }
            else
            {
                return SearchPath(dir, i - 1, j - 1) + s1[i - 1] + s2[j - 1];
            }
        }
        else if (dir[i][j] == 1)
        {
            return SearchPath(dir, i - 1, j) + s1[i - 1];
        }
        else
        {
            return SearchPath(dir, i, j - 1) + s2[j - 1];
        }
    }

    string shortestCommonSupersequence(string str1, string str2)
    {
        s1 = str1;
        s2 = str2;
        int n = str1.size();
        int m = str2.size();
        vector<vector<int>> dp(n + 1, vector<int>(m + 1, 0));
        vector<vector<int>> dir(n + 1, vector<int>(m + 1, 0));
        for (int i = 1; i <= n; i++)
        {
            dp[i][0] = dp[i - 1][0] + 1;
            dir[i][0] = 1;
        }
        for (int j = 1; j <= m; j++)
        {
            dp[0][j] = dp[0][j - 1] + 1;
            dir[0][j] = 2;
        }
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                if (str1[i - 1] == str2[j - 1])
                {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                }
                else
                {
                    dp[i][j] = dp[i - 1][j - 1] + 2;
                }
                int tmp = dp[i - 1][j] + 1;
                if (tmp < dp[i][j])
                {
                    dp[i][j] = tmp;
                    dir[i][j] = 1;
                }
                tmp = dp[i][j - 1] + 1;
                if (tmp < dp[i][j])
                {
                    dp[i][j] = tmp;
                    dir[i][j] = 2;
                }
            }
        }
        return SearchPath(dir, n, m);
    }
};

int main()
{
    Solution s;
    string str1 = "abac";
    string str2 = "cabc";
    string res = s.shortestCommonSupersequence(str1, str2);
    cout << res;
}