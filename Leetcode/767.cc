#include <algorithm>
#include <iostream>
#include <queue>
#include <string>
#include <vector>
using namespace std;

class Solution
{
public:
    string reorganizeString(string S)
    {
        vector<int> sc(26, 0);
        for (auto c : S)
            sc[c - 'a']++;

        auto cmp = [&](const char &c1, const char &c2) {
            return sc[c1 - 'a'] < sc[c2 - 'a'];
        };
        priority_queue<char, vector<char>, decltype(cmp)> q{cmp};

        int max = 0;
        for (int i = 0; i != 26; i++)
        {
            if (sc[i] > 0)
            {
                q.push('a' + i);
                if (sc[i] > max)
                    max = sc[i];
            }
        }
        if (max > (int)(((float)S.size() / 2) + 0.5))
            return "";

        string ret;
        while (!q.empty())
        {
            char c1 = q.top();
            q.pop();
            sc[c1 - 'a']--;
            ret.push_back(c1);

            if (q.empty())
                break;

            char c2 = q.top();
            q.pop();
            sc[c2 - 'a']--;
            ret.push_back(c2);

            if (sc[c1 - 'a'] > 0)
                q.push(c1);
            if (sc[c2 - 'a'] > 0)
                q.push(c2);
        }
        return ret;
    }

#define forp(i, s, n) for (int i = (s); i < (n); i++)

    string resolve(string S)
    {
        //记录字母数并降序排列
        vector<pair<char, int>> h(26, make_pair('a', 0));

        for (int i = 0; i < 26; i++)
            h[i].first += i;
        for (char p : S)
            h[p - 'a'].second++;
        sort(h.begin(), h.end(), [](pair<char, int> a, pair<char, int> b) { return a.second > b.second; });

        //判断是否能够重构
        if (h[0].second > (S.size() + 1) / 2)
            return "";

        //答案有size个字符串拼接而成，轮转排入。
        int size = h[0].second;
        vector<string> ans(size);
        int l = 0;

        forp(i, 0, 26)
        {
            int n = h[i].second;
            forp(j, 0, n)
            {
                ans[l++ % size].push_back(h[i].first);
            }
        }

        string res;
        forp(i, 0, size)
        {
            res += ans[i];
        }

        return res;
    }
};

int main()
{
    Solution solu;
    string s{"aaababaacbb"};
    solu.resolve(s);
}