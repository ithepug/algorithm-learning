#include <iostream>
#include <vector>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution
{
public:
    TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder)
    {
        if (preorder.size() < 1)
            return nullptr;
        return build(preorder, inorder, 0, preorder.size() - 1, 0, inorder.size() - 1);
    }

    TreeNode *build(vector<int> &preorder, vector<int> &inorder, int p_start, int p_end, int i_start, int i_end)
    {
        if (p_start == p_end)
        {
            return new TreeNode(preorder[p_start]);
        }
        else if (p_start > p_end)
        {
            return nullptr;
        }
        TreeNode *root = new TreeNode(preorder[p_start]);
        int index = findValue(inorder, preorder[p_start]);
        root->left = build(preorder, inorder, p_start + 1, p_start + (index - i_start),
                           i_start, index - 1);
        root->right = build(preorder, inorder, p_start + (index - i_start) + 1, p_end,
                            index + 1, i_end);
        return root;
    }

    int findValue(const vector<int> &order, const int &v)
    {
        for (int i = 0; i != order.size(); i++)
        {
            if (v == order[i])
                return i;
        }
        return -1;
    }
};

int main()
{
    Solution s;
    vector<int> v1 = {1, 2, 3};
    vector<int> v2 = {3, 2, 1};
    s.buildTree(v1, v2);
}