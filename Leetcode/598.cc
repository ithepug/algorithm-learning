#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
 public:
  int maxCount(int m, int n, vector<vector<int>>& ops) {
    auto min_m = m;
    auto min_n = n;

    for (const auto& op : ops) {
      min_m = min(min_m, op[0]);
      min_n = min(min_n, op[1]);
    }

    return min_m * min_n;
  }
};