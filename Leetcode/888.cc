#include <iostream>
#include <numeric>
#include <unordered_set>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<int> fairCandySwap(vector<int> &A, vector<int> &B)
    {
        int sumA = accumulate(A.begin(), A.end(), 0);
        int sumB = accumulate(B.begin(), B.end(), 0);
        int target = (sumA + sumB) >> 1;
        unordered_set<int> uset(B.begin(), B.end());
        vector<int> ret;
        for (auto &x : A)
        {
            int d = target - (sumB + x);
            if (uset.find(-d) != uset.end())
            {
                ret.push_back(x);
                ret.push_back(-d);
                break;
            }
        }
        return ret;
    }
};

int main()
{
}