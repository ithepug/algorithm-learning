#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int maxProduct(vector<int> &nums)
    {
        if (nums.size() < 2)
            return nums[0];
        int themax = nums[0];
        int pre = nums[0];
        for (int i = 1; i != nums.size(); i++)
        {
            pre = max(pre * nums[i], nums[i]);
            themax = max(pre, themax);
        }
        return themax;
    }
};