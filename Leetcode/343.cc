class Solution {
public:
    int integerBreak(int n)
    {
        vector dp(n + 1, 0);
        if (n < 2)
            return 1;
        for (int i = 2; i <= n; i++)
        {
            int theMax = 0;
            for (int j = 1; j < i; j++)
            {
                theMax = max(theMax, max(j * (i - j), j * dp[i - j]));
            }
            dp[i] = theMax;
        }
        return dp[n];
    }
};