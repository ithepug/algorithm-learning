#include <iostream>
#include <stack>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<int> dailyTemperatures(vector<int> &T)
    {
        int n = T.size();
        vector<int> ret(n, 0);
        stack<int> st;
        st.push(0);
        for (int i = 1; i != n; i++)
        {
            while (!st.empty() && T[i] > T[st.top()])
            {
                ret[st.top()] = i - st.top();
                st.pop();
            }
            st.push(i);
        }
        return ret;
    }
};

int main()
{
    Solution s;
    vector<int> v = {73, 74, 75, 71, 69, 72, 76, 73};
    s.dailyTemperatures(v);
}