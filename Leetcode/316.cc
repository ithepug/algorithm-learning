#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    string removeDuplicateLetters(string s)
    {
        int amount[26] = {0};
        int has[26] = {0};

        for (auto ch : s)
        {
            amount[ch - 'a']++;
        }

        string stk;
        for (auto ch : s)
        {
            while (!stk.empty() && ch <= stk.back())
            {
                if (amount[stk.back() - 'a'] < 1 || has[ch - 'a'] ==1)
                    break;
                has[stk.back() - 'a'] = 0;
                stk.pop_back();
            }
            amount[ch - 'a']--;
            if (has[ch - 'a'] == 0)
            {
                stk.push_back(ch);
                has[ch - 'a'] = 1;
            }
        }
        return stk;
    }
};

int main()
{
    Solution s;
    s.removeDuplicateLetters("bbcaac");
}