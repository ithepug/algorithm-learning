#include <iostream>
#include <unordered_map>
using namespace std;

class Node
{
public:
    int val;
    Node *next;
    Node *random;

    Node(int _val)
    {
        val = _val;
        next = NULL;
        random = NULL;
    }
};

class Solution
{
private:
    unordered_map<Node *, Node *> mp;

public:
    Node *copyRandomList(Node *head)
    {
        return getCopyNode(head);
    }

    Node *getCopyNode(Node *addr)
    {
        if(addr == nullptr)
            return nullptr;
        if (mp.find(addr) == mp.end())
        {
            mp[addr] = new Node(addr->val);
            mp[addr]->next = getCopyNode(addr->next);
            mp[addr]->random = getCopyNode(addr->random);
        }
        return (mp.find(addr))->second;
    }
};

int main()
{
    Solution s;
}