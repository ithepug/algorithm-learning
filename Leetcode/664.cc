#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int strangePrinter(string s)
    {
        int n = s.size();
        vector<vector<int>> dp(n, vector<int>(n, 1));
        for (int i = n - 1; i >= 0; i--)
        {
            for (int j = i + 1; j < n; j++)
            {
                if (s[i] == s[j])
                {
                    dp[i][j] = dp[i][j - 1];
                }
                else
                {
                    int themin = INT_MAX;
                    for (int k = i; k != j; k++)
                    {
                        themin = min(themin, dp[i][k] + dp[k + 1][j]);
                    }
                    dp[i][j] = themin;
                }
            }
        }
        return dp[0][n - 1];
    }
};

int main()
{
    Solution s;
    s.strangePrinter("aaabbb");
}