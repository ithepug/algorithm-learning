#include <iostream>
using namespace std;

class Solution
{
public:
    int trailingZeroes(int n)
    {
        int cnt = 0;
        while (n)
        {
            cnt += n / 5;
            n /= 5;
        }
        return cnt;
    }
};

int main()
{
    Solution s;
    int ans = s.trailingZeroes(27);
    cout << ans;
}