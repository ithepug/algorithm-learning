#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int firstUniqChar(string s)
    {
        int a[26] = {0};
        vector<int> v;
        for (int i = 0; i != s.size(); i++)
        {
            if (a[s[i] - 'a'] == 0)
                v.push_back(i);
            a[s[i] - 'a']++;
        }
        for (int i = 0; i != v.size(); i++)
        {
            if(a[s[v[i]] - 'a'] == 1)
                return v[i];
        }
        return -1;
    }
};

int main()
{
    Solution s;
    s.firstUniqChar("leetcode");
}