#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int rob(vector<int> &nums)
    {
        if (nums.size() < 2)
            return nums[0];
        vector<int> v1(nums.begin() + 1, nums.end());
        int dp1 = dpp(v1);
        vector<int> v2(nums.begin(), nums.end() - 1);
        int dp2 = dpp(v2);
        return max(dp1, dp2);
    }

    int dpp(vector<int> &nums)
    {
        int n = nums.size();
        if (n < 3)
            return *max_element(nums.begin(), nums.end());
        vector<int> dp(n, 0);
        dp[0] = nums[0];
        dp[1] = max(nums[0], nums[1]);
        for (int i = 2; i != n; i++)
        {
            dp[i] = max(dp[i - 2] + nums[i], dp[i - 1]);
        }
        return dp[n - 1];
    }
};

int main()
{
    vector<int> v({0});
    Solution s;
    s.rob(v);
}