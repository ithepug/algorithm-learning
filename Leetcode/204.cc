#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int countPrimes(int n) {
        vector<int> v(n,1);
        int ret = 0;
        for(int i=2;i<n;i++)
        {
            if(v[i])
            {
                ret++;
                for(long long j = (long long)i*i;j<n;j+=i)
                {
                    v[j]=0;
                }
            }
        }
        return ret;
    }
};
int main()
{
}