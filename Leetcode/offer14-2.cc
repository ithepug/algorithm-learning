#include <cmath>
#include <iostream>
using namespace std;

class Solution
{
public:
    int cuttingRope(int n)
    {
        if (n < 3)
            return n - 1;
        int c = n / 3;
        int r = n % 3;
        if (r == 0)
        {
            return myPow(3, c);
        }
        else if (r == 1)
        {
            return ((long long)myPow(3, c - 1) * 4) % 1000000007;
        }
        else //2
        {
            return ((long long)myPow(3, c) * 2) % 1000000007;
        }
    }

    int myPow(const int &left, const int &right) const
    {
        int ret = 1;
        for (int i = 0; i != right; i++)
        {
            ret = ((long long)ret * left) % 1000000007;
        }
        return ret;
    }
};

int main()
{
    Solution s;
    s.cuttingRope(553);
}