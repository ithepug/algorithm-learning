#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    bool isAng(vector<int> &A, int lastonerank)
    {
        int a = A[lastonerank];
        int b = A[lastonerank - 1];
        int c = A[lastonerank - 2];
        if (b + c <= a)
            return false;
        return true;
    }

    int largestPerimeter(vector<int> &A)
    {
        sort(A.begin(), A.end());
        int size = A.size();
        for (size--; size >= 2; size--)
        {
            if (isAng(A, size))
                return A[size] + A[size - 1] + A[size - 2];
        }
        return 0;
    }
};

int main()
{
    vector<int> v{3, 6, 2, 3};
    Solution s;
    s.largestPerimeter(v);
}