#include <algorithm>
#include <iostream>
#include <set>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<vector<int>> subsetsWithDup(vector<int> &nums)
    {
        set<vector<int>> ans;
        for (int i = 0; i != 1 << nums.size(); i++)
        {
            vector<int> tmp;
            for (int j = 0; j != nums.size(); j++)
            {
                if (i & (1 << j))
                {
                    tmp.push_back(nums[j]);
                }
            }
            ans.insert(tmp);
        }
        return vector<vector<int>>(ans.begin(), ans.end());
    }
};

int main()
{
    Solution s;
    vector<int> nums = {1, 2, 2};
    s.subsetsWithDup(nums);
}