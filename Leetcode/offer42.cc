#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int maxSubArray(vector<int> &nums)
    {
        int n = nums.size();
        vector<int> dp(nums.begin(), nums.end());
        int ret = dp[0];
        for (int i = 1; i < n; i++)
        {
            if (dp[i - 1] > 0)
                dp[i] += dp[i - 1];
            ret = max(ret, dp[i]);
        }
        return ret;
    }
};