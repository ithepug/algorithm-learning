#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
public:
    int findShortestSubArray(vector<int> &nums)
    {
        vector<int> cnt(50000, 0);
        vector<int> left(50000, -1);
        vector<int> right(50000, -1);
        for (int i = 0; i != nums.size(); i++)
        {
            cnt[nums[i]]++;
            left[nums[i]] = left[nums[i]] == -1 ? i : left[nums[i]];
            right[nums[i]] = i;
        }
        int max = 0;
        int min_dis = INT_MAX;
        for (int i = 0; i != nums.size(); i++)
        {
            if (cnt[nums[i]] > max)
            {
                max = cnt[nums[i]];
                min_dis = right[nums[i]] - left[nums[i]] + 1;
            }
            else if (cnt[nums[i]] == max)
            {
                if ((right[nums[i]] - left[nums[i]] + 1) < min_dis)
                    min_dis = right[nums[i]] - left[nums[i]] + 1;
            }
        }
        return min_dis;
    }
};

int main()
{
    Solution s;
}