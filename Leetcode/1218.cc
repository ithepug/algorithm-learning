#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
public:
    int longestSubsequence(vector<int> &arr, int difference)
    {
        int base = 1e5;
        vector<int> dp(2e5, 0);
        int n = arr.size();
        int themax = 1;
        for (int i = 0; i != n; i++)
        {
            int idx = arr[i] + base;
            int tmp = dp[idx - difference] + 1;
            dp[idx] = tmp;
            themax = max(themax, tmp);
        }
        return themax;
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, 5, 7, 8, 5, 3, 4, 2, 1};
    cout << LLONG_MAX;
    s.longestSubsequence(v, -2);
}