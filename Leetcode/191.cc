class Solution {
public:
    int hammingWeight(uint32_t n) {
        uint32_t tmp = n;
        int cnt = 0;
        while(tmp != 0)
        {
            if(tmp & 1 == 1)
                cnt++;
            tmp >>= 1;
        }
        return cnt;
    }
};