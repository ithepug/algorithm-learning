#include <iostream>

using namespace std;

class Solution
{
public:
    int fib(int n)
    {
        int p = 0;
        int q = 1;
        int r;
        if (n == 0)
            return p;
        if (n == 1)
            return q;
        for (int i = 2; i <= n;i++)
        {
            r = ((long long)p + q) % 1000000007;
            p = q;
            q = r;
        }
        return r;
    }
};

int main()
{
    Solution s;
    s.fib(50);
}