#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<int> nextGreaterElements(vector<int> &nums)
    {
        int size = nums.size();
        vector<int> ret(size, -1);
        vector<int> tmp_s;
        if (size < 2)
            return ret;
        tmp_s.push_back(0);
        for (int i = 1; i != size << 1; i++)
        {
            while (!tmp_s.empty() && nums[tmp_s.back()] < nums[i % size])
            {
                ret[tmp_s.back()] = nums[i % size];
                tmp_s.pop_back();
            }
            tmp_s.push_back(i % size);
        }
        return ret;
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, 2, 1};
    s.nextGreaterElements(v);
}