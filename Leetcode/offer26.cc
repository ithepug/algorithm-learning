#include <iostream>
#include <queue>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
    TreeNode(int x, TreeNode *l, TreeNode *r) : val(x), left(l), right(r) {}
};

class Solution
{
public:
    bool isSubStructure(TreeNode *A, TreeNode *B)
    {
        if (B == nullptr)
            return false;
        queue<TreeNode *> q;
        q.push(A);
        while (!q.empty())
        {
            TreeNode *node = q.front();
            q.pop();
            if (isSomeTree(node, B))
                return true;
            if (node->left != nullptr)
                q.push(node->left);
            if (node->right != nullptr)
                q.push(node->right);
        }
        return false;
    }

    bool isSomeTree(TreeNode *A, TreeNode *B)
    {
        // nullptr
        if (A == B)
        {
            return true;
        }
        else if (A == nullptr)
        {
            return false;
        }
        else if (B == nullptr)
        {
            return true;
        }
        return A->val == B->val && isSomeTree(A->left, B->left) && isSomeTree(A->right, B->right);
    }
};

int main()
{
    Solution s;
    TreeNode *n1 = new TreeNode(1);
    TreeNode *n2 = new TreeNode(2);
    TreeNode *n3 = new TreeNode(4, n1, n2);
    TreeNode *n4 = new TreeNode(5);
    TreeNode *n5 = new TreeNode(3, n3, n4);

    TreeNode *n6 = new TreeNode(1);
    TreeNode *n7 = new TreeNode(4, n6, nullptr);

    s.isSubStructure(n5, n7);
}