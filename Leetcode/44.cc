#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    bool isMatch(string s, string p)
    {
        int n = s.size();
        int m = p.size();
        vector<vector<int>> dp(n + 1, vector<int>(m + 1, 0));
        vector<int> flag(m + 1, 0);
        dp[0][0] = 1;
        flag[0] = 1;
        for (int j = 1; j <= m; j++)
        {
            if (p[j - 1] == '*')
            {
                dp[0][j] = flag[j - 1];
                flag[j] |= dp[0][j];
            }
        }
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                if (p[j - 1] == '?')
                {
                    dp[i][j] = dp[i - 1][j - 1];
                    flag[j] |= dp[i][j];
                }
                else if (p[j - 1] == '*')
                {
                    dp[i][j] = flag[j - 1];
                    flag[j] |= dp[i][j];
                }
                else
                {
                    if (s[i - 1] == p[j - 1])
                    {
                        dp[i][j] = dp[i - 1][j - 1];
                        flag[j] |= dp[i][j];
                    }
                }
            }
        }
        return dp[n][m];
    }
};

int main()
{
    Solution s;
    s.isMatch("aa", "a*");
}