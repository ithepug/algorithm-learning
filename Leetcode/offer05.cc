#include <iostream>
#include <string>
using namespace std;

class Solution
{
public:
    string replaceSpace(string s)
    {
        string ret;
        for (auto c : s)
        {
            if (c == ' ')
            {
                ret += "%20";
            }
            else
            {
                ret += c;
            }
        }
        return ret;
    }
};

int main()
{
}