#include <iostream>
#include <queue>
#include <vector>
using namespace std;

class Solution
{
public:
    int orangesRotting(vector<vector<int>> &grid)
    {
        int n = grid.size();
        if (n < 1)
            return -1;
        int m = grid[0].size();

        vector<vector<int>> dis(n, vector<int>(m, -1));
        queue<pair<int, int>> q;
        int cnt = 0;

        for (int i = 0; i != n; i++)
            for (int j = 0; j != m; j++)
                if (grid[i][j] == 2)
                {
                    q.push({i, j});
                    dis[i][j] = 0;
                }
                else if (grid[i][j] == 1)
                    cnt++;
        int ans = 0;
        int xd[] = {1, -1, 0, 0};
        int yd[] = {0, 0, 1, -1};
        while (!q.empty())
        {
            pair<int, int> node = q.front();
            q.pop();
            for (int i = 0; i != 4; i++)
            {
                int x = node.first + xd[i];
                int y = node.second + yd[i];
                if (x >= 0 && x < n && y >= 0 && y < m)
                    if (dis[x][y] == -1 && grid[x][y] == 1)
                    {
                        ans = dis[x][y] = dis[node.first][node.second] + 1;
                        grid[x][y] = 2;
                        q.push({x, y});
                        if (!--cnt)
                            return ans;
                    }
            }
        }
        return cnt ? -1 : ans;
    }
};

int main()
{
    cout << bool(!0);
    system("pause");
}