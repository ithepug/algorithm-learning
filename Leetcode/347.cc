#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
public:
    static bool cmp(const pair<int, int> &lhs, const pair<int, int> &rhs)
    {
        return lhs.second > rhs.second;
    }

    vector<int> topKFrequent(vector<int> &nums, int k)
    {
        unordered_map<int, int> mp;
        for (auto x : nums)
            mp[x]++;
        vector<pair<int, int>> vv(mp.begin(), mp.end());
        sort(vv.begin(), vv.end(), cmp);
        vector<int> ret(k);
        auto it = vv.begin();
        for (int i = 0; i != k; i++)
        {
            ret[i] = (*it).first;
            it++;
        }
        return ret;
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, 1, 1, 2, 3, 2};
    s.topKFrequent(v, 2);
}