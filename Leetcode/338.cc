#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<int> countBits(int num)
    {
        vector<int> ret(num + 1);
        ret[0] = 0;
        int tmp = 1;
        for (int i = 1; i <= num; i++)
        {
            if (i > tmp)
            {
                tmp <<= 1;
            }
            if (i == tmp)
            {
                ret[i] = 1;
            }
            else
            {
                ret[i] = ret[i - (tmp >> 1)] + 1;
            }
        }
        return ret;
    }
};

int main()
{
    Solution s;
    s.countBits(16);
}