#include <iostream>
#include <numeric>
#include <vector>
using namespace std;

class Solution
{
public:
    bool canPartition(vector<int> &nums)
    {
        int n = nums.size();
        if (n < 2)
            return false;
        int sum = accumulate(nums.begin(), nums.end(), 0);
        if (sum & 1)
            return false;
        int target = sum >> 1;
        vector<vector<bool>> dp(n, vector<bool>(sum, false));
        for (int i = 0; i != n; i++)
            dp[i][0] = true;
        dp[0][nums[0]] = true;
        for (int i = 1; i != n; i++)
        {
            for (int j = 1; j != target + 1; j++)
            {
                if (nums[i] > j)
                {
                    dp[i][j] = dp[i - 1][j];
                }
                else
                {
                    dp[i][j] = dp[i - 1][j] | dp[i - 1][j - nums[i]];
                }
            }
        }
        return dp[n - 1][target];
    }
};

int main()
{
    vector<int> v = {99, 1};
    Solution s;
    s.canPartition(v);
}