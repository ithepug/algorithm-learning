#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int maxSizeSlices(vector<int> &slices)
    {
        int size = slices.size();
        int n = size / 3;
        vector<vector<int>> dp(size + 2, vector<int>(n + 1, 0));
        // 首个不选
        for (int i = 3; i != size + 2; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                dp[i][j] = max(dp[i - 2][j - 1] + slices[i - 2], dp[i - 1][j]);
            }
        }
        int tmp = dp[size + 1][n];
        dp.assign(size + 2, vector<int>(n + 1, 0));
        // 最后一个不选
        for (int i = 2; i != size + 1; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                dp[i][j] = max(dp[i - 2][j - 1] + slices[i - 2], dp[i - 1][j]);
            }
        }
        return max(tmp, dp[size][n]);
    }
};

int main()
{
    Solution s;
    vector<int> v = {9,5,1,7,8,4,4,5,5,8,7,7};
    cout << s.maxSizeSlices(v);
    system("pause");
}