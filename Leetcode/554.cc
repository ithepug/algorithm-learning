#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
public:
    int leastBricks(vector<vector<int>> &wall)
    {
        unordered_map<int, int> mp;
        for (auto w : wall)
        {
            int sum = 0;
            for (int i = 0; i != w.size() - 1; i++)
            {
                sum += w[i];
                if (mp.find(sum) != mp.end())
                {
                    mp[sum]++;
                }
                else
                {
                    mp[sum] = 1;
                }
            }
        }
        int max_e = INT_MIN;
        for (auto x : mp)
            max_e = max(max_e, x.second);
        return wall.size() - max_e;
    }
};

int main()
{
    vector<vector<int>> vv = {{}};
    Solution s;
    s.leastBricks(vv);
}