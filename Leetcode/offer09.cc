#include <iostream>
#include <stack>

using namespace std;

class CQueue
{
public:
    CQueue()
    {
    }

    void appendTail(int value)
    {
        stack1.push(value);
    }

    int deleteHead()
    {
        int ret = -1;
        if (!stack2.empty())
        {
            ret = stack2.top();
            stack2.pop();
        }
        else
        {
            while (!stack1.empty())
            {
                stack2.push(stack1.top());
                stack1.pop();
            }
            if (!stack2.empty())
            {
                ret = stack2.top();
                stack2.pop();
            }
        }
        return ret;
    }

private:
    stack<int> stack1;
    stack<int> stack2;
};

int main()
{
    CQueue cqueue;
    cqueue.deleteHead();
    cqueue.appendTail(12);
    cqueue.deleteHead();
    cqueue.appendTail(10);
    cqueue.appendTail(9);
    cqueue.deleteHead();
    cqueue.deleteHead();
    cqueue.deleteHead();
}