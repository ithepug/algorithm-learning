#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<vector<int>> generate(int numRows)
    {
        vector<vector<int>> A(numRows);
        for (int i = 0; i != numRows; i++) // 行
        {
            vector<int> tmp(i + 1, 1);
            for (int j = 1; j < i; j++) // 列
                tmp[j] = A[i - 1][j - 1] + A[i - 1][j];
            A[i] = tmp;
        }
        return A;
    }
};

int main()
{
    Solution s;
    s.generate(5);
}