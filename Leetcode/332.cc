#include <algorithm>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
private:
    unordered_map<string, priority_queue<string, vector<string>, greater<string>>> G;
    vector<string> ans;
    int size;

public:
    void dfs(string node)
    {
        while (!G[node].empty())
        {
            string tmp = G[node].top();
            G[node].pop();
            dfs(tmp);
        }
        ans.push_back(node);
    }

    vector<string> findItinerary(vector<vector<string>> &tickets)
    {
        size = tickets.size();
        for (auto &t : tickets)
        {
            G[t[0]].emplace(t[1]);
        }
        dfs("JFK");
        reverse(ans.begin(), ans.end());
        return ans;
    }
};

int main()
{
    Solution s;
    vector<vector<string>> v = {{"JFK", "SFO"}, {"JFK", "ATL"}, {"SFO", "ATL"}, {"ATL", "JFK"}, {"ATL", "SFO"}};
    vector<vector<string>> v2 = {{"MUC", "LHR"}, {"JFK", "MUC"}, {"SFO", "SJC"}, {"LHR", "SFO"}};
    s.findItinerary(v);
    return 0;
}