#include <iostream>
#include <vector>
using namespace std;

class Solution
{
private:
    int first;
    int second;

public:
    void FindDivisor(int num)
    {
        for (int i = 1; i * i <= num; i++)
        {
            if (num % i == 0)
            {
                if (abs(i - num / i) < abs(first - second))
                {
                    first = i;
                    second = num / i;
                }
            }
        }
    }

    vector<int> closestDivisors(int num)
    {
        first = 1;
        second = INT_MAX;
        FindDivisor(num + 1);
        FindDivisor(num + 2);
        return {first, second};
    }
};