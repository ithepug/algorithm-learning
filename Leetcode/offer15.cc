#include <iostream>
using namespace std;

class Solution
{
public:
    int hammingWeight(uint32_t n)
    {
        uint32_t t = 1;
        int cnt = 0;
        for (int i = 0; i != 32; i++)
        {
            if (n & t)
                cnt++;
            t <<= 1;
        }
        return cnt;
    }
};

int main()
{
    Solution s;
    s.hammingWeight(11);
}