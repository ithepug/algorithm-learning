#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
public:
    int minFlipsMonoIncr(string s)
    {
        if (!s.size())
            return 0;

        int min0 = s[0] == '0' ? 0 : 1;
        int min1 = 0;
        for (int i = 1; i != s.size(); i++)
        {
            min1 = min(min0, min1) + (s[i] == '0' ? 1 : 0);
            min0 = min0 + (s[i] == '1' ? 1 : 0);
            cout << "[0, " << i << "]" << min0 << " " << min1 << endl;
        }
        return min(min0, min1);
    }
};

int main()
{
    Solution s;
    cout << s.minFlipsMonoIncr("00011000") << endl;
    system("pause");
}