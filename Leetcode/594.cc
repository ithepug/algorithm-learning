#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
public:
    int findLHS(vector<int> &nums)
    {
        unordered_map<int, int> mp;
        for (auto n : nums)
        {
            mp[n]++;
        }
        int themax = 0;
        for (auto n : nums)
        {
            if (mp.count(n + 1))
                themax = max(themax, mp[n] + mp[n + 1]);
        }
        return themax;
    }
};