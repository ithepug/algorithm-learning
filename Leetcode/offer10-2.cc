#include <iostream>
using namespace std;

class Solution
{
public:
    int numWays(int n)
    {
        int p = 1;
        int q = 2;
        int r;
        switch (n)
        {
        case 0:
            return 1;
        case 1:
            return p;
        case 2:
            return q;
        default:
            while (n-- > 2)
            {
                r = ((long long)p + q) % 1000000007;
                p = q;
                q = r;
            }
            return r;
        }
    }
};

int main()
{
    Solution s;
    s.numWays(3);
}