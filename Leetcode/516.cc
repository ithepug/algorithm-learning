#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int longestPalindromeSubseq(string s)
    {
        int n = s.size();
        vector<vector<int>> dp(n, vector<int>(n, 1));
        for (int i = 0; i + 1 < n; i++)
            if (s[i] == s[i + 1])
                dp[i][i + 1] = 2;
        for (int l = 2; l < n; l++)
        {
            for (int i = 0; l + i < n; i++)
            {
                if (s[i] == s[i + l])
                    dp[i][i + l] = dp[i + 1][i + l - 1] + 2;
                else
                    dp[i][i + l] = max(dp[i + 1][i + l], dp[i][i + l - 1]);
            }
        }
        return dp[0][n - 1];
    }
};