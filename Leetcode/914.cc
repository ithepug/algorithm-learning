#include <iostream>
#include <numeric>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
public:
    bool hasGroupsSizeX(vector<int> &deck)
    {
        unordered_map<int, int> cnt;
        for (int c : deck)
        {
            cnt[c]++;
        }
        int g = (*cnt.begin()).second;
        for (auto c : cnt)
        {
            g = gcd(g, c.second);
        }
        return g >= 2;
    }
};