#include <algorithm>
#include <iostream>
#include <unordered_map>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution
{
private:
    unordered_map<TreeNode *, int> pick;
    unordered_map<TreeNode *, int> unpick;

public:
    int rob(TreeNode *root)
    {
        dfs(root);
        return max(pick[root], unpick[root]);
    }

    void dfs(TreeNode *node)
    {
        if (node == nullptr)
            return;
        dfs(node->left);
        dfs(node->right);
        pick[node] = node->val + unpick[node->left] + unpick[node->right];
        unpick[node] = max(pick[node->left], unpick[node->left]) + max(pick[node->right], unpick[node->right]);
    }
};