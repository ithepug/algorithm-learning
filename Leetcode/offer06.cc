#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
public:
    vector<int> reversePrint(ListNode *head)
    {
        // 返回链表
        
        // ListNode *node1 = nullptr;
        // ListNode *node2 = nullptr;
        // ListNode *node3 = nullptr;
        // node3 = head;
        // while (node3 != nullptr)
        // {
        //     node1 = node2;
        //     node2 = node3;
        //     node3 = node3->next;
        //     node2->next = node1;
        // }
        // return node2;

        vector<int> ret;
        ListNode *node = head;
        while (node != nullptr)
        {
            ret.push_back(node->val);
            node = node->next;
        }
        reverse(ret.begin(), ret.end());
        return ret;
    }
};

int main()
{
}