#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
private:
    vector<int> colors;
    vector<vector<int>> edge;

public:
    bool dfs(int node, int color)
    {
        if (colors[node] != -1)
            return colors[node] == color;
        colors[node] = color;
        for (int e : edge[node])
        {
            if (!dfs(e, color ^ 1))
                return false;
        }
        return true;
    }

    bool possibleBipartition(int N, vector<vector<int>> &dislikes)
    {
        edge.assign(N + 1, vector(0, 0));
        colors.assign(N + 1, -1);
        for (auto &node : dislikes)
        {
            edge[node[0]].push_back(node[1]);
            edge[node[1]].push_back(node[0]);
        }

        for (int i = 1; i != N + 1; i++)
        {
            if (colors[i] == -1 && !dfs(i, 0))
                return false;
        }
        return true;
    }
};