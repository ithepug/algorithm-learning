#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<int> spiralOrder(vector<vector<int>> &matrix)
    {
        vector<int> ret;
        int n = matrix.size();
        if (n < 1)
            return ret;
        int m = matrix[0].size();
        int size = n * m;
        ret.assign(size, 0);

        int rl = 0;
        int rr = m - 1;
        int rp = 0;
        int dl = 0;
        int dr = n - 1;
        int dp = m - 1;
        int ll = m - 1;
        int lr = 0;
        int lp = n - 1;
        int ul = n - 1;
        int ur = 0;
        int up = 0;
        for (int i = 0; i != size;)
        {
            // >>>>>>
            for (int j = rl; i != size && j <= rr; j++)
            {
                ret[i++] = matrix[rp][j];
            }
            rp++;
            dl++;
            ur++;

            // VVVVVV
            for (int j = dl; j <= dr && i != size; j++)
            {
                ret[i++] = matrix[j][dp];
            }
            dp--;
            rr--;
            ll--;

            // <<<<<<
            for (int j = ll; j >= lr && i != size; j--)
            {
                ret[i++] = matrix[lp][j];
            }
            lp--;
            ul--;
            dr--;

            // ^^^^^^
            for (int j = ul; j >= ur && i != size; j--)
            {
                ret[i++] = matrix[j][up];
            }
            up++;
            rl++;
            lr++;
        }
        return ret;
    }
};