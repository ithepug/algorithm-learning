#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    bool lemonadeChange(vector<int>& bills) {
        int A[2]={0};
        for(auto bill:bills)
        {
            switch(bill)
            {
                case 5:
                    A[0] ++;
                    break;
                case 10:
                    A[1] ++;
                    A[0] --;
                    if(A[0]<0)
                        return false;
                    break;
                case 20:
                    if(A[1]>0)
                    {    
                        A[1] --;
                        A[0] --;
                        if(A[0] < 0)
                            return false;
                    }
                    else
                    {
                        A[0] -= 3;
                        if(A[0] < 0)
                            return false;
                    }
            }
        }
        return true;
    }
};

int main()
{
    Solution s;
    vector<int> v {5,5,5,5,20,20,5,5,20,5};
    s.lemonadeChange(v);
}