#include <iostream>
#include <vector>
using namespace std;

class Solution
{
private:
    vector<string> ret;
    string ans;

public:
    vector<string> generateParenthesis(int n)
    {
        search(n, 0);
        return ret;
    }
    void search(const int &n, int cnt)
    {
        if (ans.size() == n << 1)
        {
            if (cnt == 0)
                ret.push_back(ans);
            return;
        }
        if (cnt >= 0)
        {
            ans.push_back('(');
            cnt++;
            search(n, cnt);
            ans.pop_back();
            cnt--;

            ans.push_back(')');
            cnt--;
            search(n, cnt);
            ans.pop_back();
            cnt++;
        }
    }
};

int main()
{
    Solution s;
    s.generateParenthesis(3);
}