#include <iostream>
#include <queue>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<int> findMinHeightTrees(int n, vector<vector<int>> &edges)
    {
        vector<vector<int>> mm(n);
        vector<int> depth(n, 0);
        for (auto edge : edges)
        {
            mm[edge[0]].push_back(edge[1]);
            mm[edge[1]].push_back(edge[0]);
            depth[edge[0]]++;
            depth[edge[1]]++;
        }
        queue<int> q;
        for (int i = 0; i != n; i++)
            if (depth[i] == 1)
                q.push(i);
        vector<int> ret = {0};
        while (!q.empty())
        {
            ret.clear();
            int size = q.size();
            for (int i = 0; i != size; i++)
            {
                int tmp = q.front();
                q.pop();
                ret.push_back(tmp);
                depth[tmp]--;
                for (auto j : mm[tmp])
                {
                    depth[j]--;
                    if (depth[j] == 1)
                        q.push(j);
                }
            }
        }
        return ret;
    }
};

int main()
{
    vector<vector<int>> vv;
    vv.push_back({3, 0});
    vv.push_back({3, 1});
    vv.push_back({3, 2});
    vv.push_back({3, 4});
    vv.push_back({5, 4});
    Solution s;
    s.findMinHeightTrees(6, vv);
}