#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    void rotate(vector<vector<int>> &matrix)
    {
        int n = matrix.size();
        int t1, t2;

        // NOTE!!!!!!!!!!
        n = n - 1;

        for (int i = 0; i < ((n + 1) >> 1); i++)
            for (int j = i; j < (int)(0.5+(n + 1 - i) /2.0); j++)
            {
                t1 = matrix[i][j];
                matrix[i][j] = matrix[n - j][i]; //1
                t2 = matrix[0 + j][n - i];
                matrix[0 + j][n - i] = t1; //2
                t1 = matrix[n - i][n - j];
                matrix[n - i][n - j] = t2; //3
                matrix[n - j][i] = t1;     //4
            }
    }
};

int main()
{
    Solution s;
    vector<vector<int>> matrix = {{5, 1, 9, 11},
                                  {2, 4, 8, 10},
                                  {13, 3, 6, 7},
                                  {15, 14, 12, 16}};
    vector<vector<int>> matrix2 = {{1, 2},
                                   {3, 4}};
    s.rotate(matrix2);
}