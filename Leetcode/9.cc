#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    bool isPalindrome(int x)
    {
        if (x < 0)
            return false;
        vector<int> v(10);
        int tmp = x;
        int i;
        for (i = 0; tmp != 0; i++)
        {
            v[i] = tmp % 10;
            tmp /= 10;
        }
        for (int j = 0; j != i >> 1; j++)
        {
            if (v[j] != v[i - j - 1])
                return false;
        }
        return true;
    }
};

int main()
{
    Solution s;
    s.isPalindrome(1001);
}