#include <iostream>
#include <queue>
#include <vector>

using namespace std;

class Solution
{
public:
    vector<int> smallestK(vector<int> &arr, int k)
    {
        if(k == 0)
        {
            return vector<int>();
        }
        priority_queue<int, vector<int>, less<int>> pq;
        if (arr.size() < k)
        {
            return arr;
        }
        int i;
        for (i = 0; i != k; i++)
        {
            pq.push(arr[i]);
        }
        for (; i != arr.size();i++)
        {
            if(arr[i] < pq.top())
            {
                pq.pop();
                pq.push(arr[i]);
            }
        }

        vector<int> ret(k);
        for (i = 0; i != k;i++)
        {
            ret[i] = pq.top();
            pq.pop();
        }
        return ret;
    }
};

int main()
{
    vector<int> a({1,3,5,7,2,4,6,8});
    Solution s;
    s.smallestK(a, 4);
}