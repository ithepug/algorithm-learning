class Solution {
public:
    bool isMonotonic(vector<int>& A) {
        int d = 0;
        if(A.size() < 2)
            return true;
        for(int i=0;i!=A.size() - 1;i++)
        {
            if(d == 0)
            {
                d = A[i] > A[i+1] ? 1 : A[i] == A[i+1] ? 0 : -1;
            } else if(d == 1)
            {
                if(A[i] < A[i+1])
                    return false;
            } else if(d == -1){
                if(A[i] > A[i+1])
                    return false;
            }
        }
        return true;
    }
};