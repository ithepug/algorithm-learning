#include <iostream>
using namespace std;

class Node
{
public:
    int val;
    Node *left;
    Node *right;

    Node() {}

    Node(int _val)
    {
        val = _val;
        left = NULL;
        right = NULL;
    }

    Node(int _val, Node *_left, Node *_right)
    {
        val = _val;
        left = _left;
        right = _right;
    }
};

class Solution
{
private:
    Node *pre;
    Node *head;

public:
    void dfs(Node *node)
    {
        if (node == nullptr)
            return;
        dfs(node->left);
        if (pre != nullptr)
            pre->right = node;
        else
            head = node;
        node->left = pre;
        pre = node;
        dfs(node->right);
    }

    Node *treeToDoublyList(Node *root)
    {
        dfs(root);
        if (root != nullptr)
        {
            head->left = pre;
            pre->right = head;
        }
        return head;
    }
};