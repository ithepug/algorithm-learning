#include <iostream>
#include <vector>
using namespace std;
class Solution
{
public:
    vector<int> productExceptSelf(vector<int> &nums)
    {
        int n = nums.size();
        vector<int> pre(n + 1);
        vector<int> suf(n + 1);
        pre[0] = 1;
        suf[n] = 1;
        for (int i = 0; i != n; i++)
        {
            pre[i + 1] = pre[i] * nums[i];
            suf[n - i - 1] = suf[n - i] * nums[n - i - 1];
        }
        vector<int> ret(n);
        for (int i = 0; i != n; i++)
            ret[i] = pre[i] * suf[i + 1];
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, 2, 3, 4};
    s.productExceptSelf(v);
}