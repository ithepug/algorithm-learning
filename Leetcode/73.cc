#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    void setZeroes(vector<vector<int>> &matrix)
    {
        int n = matrix.size();
        if (n < 1)
            return;
        int m = matrix[0].size();

        int flag = 1;

        for (int i = 0; i != n; i++)
            for (int j = 0; j != m; j++)
            {
                if (matrix[i][j] == 0)
                {
                    matrix[i][0] = 0;
                    if (j != 0)
                        matrix[0][j] = 0;
                    else
                        flag = 0;
                }
            }
        for (int i = 1; i != n; i++)
            for (int j = 1; j != m; j++)
            {
                if (matrix[i][0] == 0 || matrix[0][j] == 0)
                    matrix[i][j] = 0;
            }

        for (int j = 0; j != m; j++)
        {
            if (matrix[0][0] == 0)
                matrix[0][j] = 0;
        }
        
        for (int i = 0; i != n; i++)
        {
            if (flag == 0)
                matrix[i][0] = 0;
        }

        return;
    }
};

int main()
{
    Solution s;
    vector<vector<int>> vv = {{1, 2, 3, 4}, {5, 0, 7, 8}, {0, 10, 11, 12}, {13, 14, 15, 0}};
    s.setZeroes(vv);
}