#include <iostream>
#include <stack>
using namespace std;

class Solution
{
public:
    bool isValidSerialization(string preorder)
    {
        int n = preorder.size();
        stack<int> s;
        s.push(1);
        for (int i = 0; i != n; i++)
        {
            if (s.empty())
                return false;
            if (preorder[i] == ',')
            {
                ;
            }
            else if (preorder[i] == '#')
            {
                s.top()--;
                if (s.top() == 0)
                {
                    s.pop();
                }
            }
            else
            {
                while (i + 1 < n && preorder[i + 1] != ',')
                {
                    i++;
                }
                s.top()--;
                if (s.top() == 0)
                {
                    s.pop();
                }
                s.push(2);
            }
        }
        return s.empty();
    }
};

int main()
{
    Solution s;
    s.isValidSerialization("9,3,4,#,#,1,#,#,2,#,6,#,#");
}