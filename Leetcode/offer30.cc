#include <algorithm>
#include <iostream>
#include <stack>
using namespace std;

class MinStack
{
private:
    stack<int> st;
    stack<int> min_st;

public:
    MinStack()
    {
        min_st.push(INT_MAX);
    }

    void push(int x)
    {
        st.push(x);
        min_st.push(::min(min_st.top(), x));
    }

    void pop()
    {
        st.pop();
        min_st.pop();
    }

    int top()
    {
        return st.top();
    }

    int min()
    {
        return min_st.top();
    }
};