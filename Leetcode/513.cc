#include <iostream>

using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution
{
private:
    int depth;
    int ans;

public:
    void dfs(TreeNode *node, int d)
    {
        if (d > depth)
        {
            depth = d;
            ans = node->val;
        }
        if (node->left != nullptr)
        {
            dfs(node->left, d + 1);
        }
        if (node->right != nullptr)
        {
            dfs(node->right, d + 1);
        }
    }

    int findBottomLeftValue(TreeNode *root)
    {
        depth = 0;
        ans = root->val;
        dfs(root, 0);
        return ans;
    }
};