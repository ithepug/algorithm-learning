#include <iostream>
#include <vector>

using namespace std;

class FUS
{
    vector<int> v_;

public:
    FUS(int n)
    {
        for (int i = 0; i != n; i++)
        {
            v_.push_back(i);
        }
    }

    int Find(int x)
    {
        return v_[x] == x ? x : Find(v_[x]);
    }

    void Union(int a, int b)
    {
        v_[Find(a)] = Find(b);
    }

    int DepSize()
    {
        int ret = 0;
        for (int i = 0; i != v_.size(); i++)
            if (v_[i] == i)
                ret++;
        return ret;
    }
};

class Solution
{
public:
    int findCircleNum(vector<vector<int>> &isConnected)
    {
        int n = isConnected.size();
        FUS fus(n);
        for (int i = 0; i != n; i++)
        {
            for (int j = 0; j != n; j++)
            {
                if (isConnected[i][j])
                {
                    fus.Union(i, j);
                }
            }
        }
        return fus.DepSize();
    }
};

int main()
{
    vector<vector<int>> v;
    vector<int> v1{1, 1, 0};
    vector<int> v2{1, 1, 0};
    vector<int> v3{0, 0, 0};
    v.push_back(v1);
    v.push_back(v2);
    v.push_back(v3);
    Solution s;
    s.findCircleNum(v);
}