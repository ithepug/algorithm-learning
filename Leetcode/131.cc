#include <iostream>
#include <vector>
using namespace std;

class Solution
{
private:
    vector<vector<bool>> dp;
    vector<vector<string>> ret;
    vector<string> ans;

public:
    vector<vector<string>> partition(string s)
    {
        int size = s.size();
        dp.assign(size, vector<bool>(size, false));
        for (int i = 0; i != size; i++)
        {
            dp[i][i] = true;
            int l = i - 1;
            int r = i + 1;
            while (l >= 0 && r < size && s[l] == s[r])
            {
                dp[l][r] = true;
                l--;
                r++;
            }
        }
        for (int i = 0; i != size; i++)
        {
            int l = i;
            int r = i + 1;
            while (l >= 0 && r < size && s[l] == s[r])
            {
                dp[l][r] = true;
                l--;
                r++;
            }
        }
        search(s, 0);
        return ret;
    }

    void search(const string &s, int start)
    {
        if (start == s.size())
        {
            ret.push_back(ans);
            return;
        }
        for (int i = start; i < s.size(); i++)
        {
            if (dp[start][i])
            {
                ans.push_back(s.substr(start, i - start + 1));
                search(s, i + 1);
                ans.pop_back();
            }
        }
    }
};

int main()
{
    Solution s;
    s.partition("aab");
}