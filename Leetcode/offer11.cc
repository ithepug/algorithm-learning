#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int minArray(vector<int> &numbers)
    {
        int lo = 0;
        int hi = numbers.size() - 1;
        while (lo < hi)
        {
            int mi = lo + ((hi - lo) >> 1);
            if (numbers[mi] > numbers[hi])
            {
                lo = mi + 1;
            }
            else if (numbers[mi] < numbers[hi])
            {
                hi = mi;
            }
            else
            {
                hi--;
            }
        }
        return numbers[lo];
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, 3, 5};
    s.minArray(v);
}