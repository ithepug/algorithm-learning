#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    string minNumber(vector<int> &nums)
    {
        vector<string> strs;
        for (auto n : nums)
            strs.push_back(to_string(n));
        sort(strs.begin(), strs.end(), cmp);
        string ret;
        for (auto str : strs)
        {
            ret += str;
        }
        return ret;
    }

    static bool cmp(const string &lhs, const string &rhs)
    {
        return lhs + rhs < rhs + lhs;
    }
};
