#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    bool canPlaceFlowers(vector<int> &flowerbed, int n)
    {
        int i = 0;
        if (flowerbed.size() > 1)
            if (flowerbed[0] == 0 && flowerbed[1] == 0)
                n--;
        while (true)
        {
            if (i >= flowerbed.size())
                break;
            if (flowerbed[i++] == 0)
            {
                if (i >= flowerbed.size())
                    break;
                if (flowerbed[i++] == 0)
                {
                    if (i >= flowerbed.size())
                    {
                        n--;
                        break;
                    }
                    if (flowerbed[i] == 0)
                        n--;
                }
            }
        }
        return n > 0 ? false : true;
    }
};

int main()
{
    Solution s;
    vector<int> v{0};
    s.canPlaceFlowers(v, 1);
}