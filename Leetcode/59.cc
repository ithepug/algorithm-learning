#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<vector<int>> generateMatrix(int n)
    {
        vector<vector<int>> ret(n, vector<int>(n));
        int nn = n * n;
        int rl = 0;
        int rr = n - 1;
        int rp = 0;
        int dl = 0;
        int dr = n - 1;
        int dp = n - 1;
        int ll = n - 1;
        int lr = 0;
        int lp = n - 1;
        int ul = n - 1;
        int ur = 0;
        int up = 0;
        for (int i = 1; i <= nn;)
        {
            for (int j = rl; i <= nn && j <= rr; j++)
            {
                ret[rp][j] = i++;
            }
            rp++;
            dl++;
            ur++;
            for (int j = dl; i <= nn && j <= dr; j++)
            {
                ret[j][dp] = i++;
            }
            dp--;
            rr--;
            ll--;
            for (int j = ll; i <= nn && j >= lr; j--)
            {
                ret[lp][j] = i++;
            }
            lp--;
            ul--;
            dr--;
            for (int j = ul; i <= nn && j >= ur; j--)
            {
                ret[j][up] = i++;
            }
            up++;
            rl++;
            lr++;
        }
        return ret;
    }
};

int main()
{
    Solution s;
    s.generateMatrix(3);
}