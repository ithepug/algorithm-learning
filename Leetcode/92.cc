#include <iostream>
#include <stack>
using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

class Solution
{
public:
    ListNode *reverseBetween(ListNode *head, int left, int right)
    {
        stack<ListNode *> st;
        ListNode *node = head;
        int i = 1;
        while (node != nullptr)
        {
            i++;
            if (i >= left)
                break;
            node = node->next;
        }
        ListNode *lastOne = node;
        if(left != 1)
            node = node->next;
        for (int j = left; j <= right; j++)
        {
            st.push(node);
            node = node->next;
        }
        ListNode *endOne = node;
        if (left == 1)
        {
            head = st.top();
            st.pop();
            lastOne = head;
        }
        else
        {
            ListNode *nextOne = st.top();
            st.pop();
            lastOne->next = nextOne;
            lastOne = lastOne->next;
        }
        while (!st.empty())
        {
            ListNode *nextOne = st.top();
            st.pop();
            lastOne->next = nextOne;
            lastOne = lastOne->next;
        }
        lastOne->next = endOne;
        return head;
    }
};

int main()
{
    ListNode *n5 = new ListNode(5);
    ListNode *n4 = new ListNode(4, n5);
    ListNode *n3 = new ListNode(3, n4);
    ListNode *n2 = new ListNode(2, n3);
    ListNode *n1 = new ListNode(1, n2);

    Solution s;
    s.reverseBetween(n1, 1, 3);
}