#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <string>
using namespace std;

class Solution
{
public:
    bool isIsomorphic(string s, string t)
    {
        unordered_map<char, int> m;
        unordered_set<char> set;
        for (int i = 0; i != s.size(); i++)
        {
            auto it = m.find(s[i]);
            if (it == m.end())
            {
                m[s[i]] = s[i] - t[i];
                if (set.find(t[i]) == set.end())
                    set.insert(t[i]);
                else
                    return false;
            }
            else if ((s[i] - t[i]) != (*it).second)
                return false;
        }
        return true;
    }
};

int main()
{
}