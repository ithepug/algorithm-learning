#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution
{
public:
    int lenLongestFibSubseq(vector<int> &arr)
    {
        int n = arr.size();
        vector<vector<int>> dp(n, vector<int>(n, 0));
        unordered_map<int, int> index;
        for (int i = 0; i != n; i++)
            index[arr[i]] = i;
        int ans = 0;
        for (int i = 0; i != n; i++)
            for (int j = 0; j < i; j++)
            {
                if (arr[i] - arr[j] < arr[j] && index.find(arr[i] - arr[j]) != index.end())
                {
                    int k = index[arr[i] - arr[j]];
                    dp[j][i] = dp[k][j] + 1;
                    ans = max(ans, dp[j][i] + 2);
                }
            }
        return ans >= 3 ? ans : 0;
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8};
    int ans = s.lenLongestFibSubseq(v);
    cout << ans;
}