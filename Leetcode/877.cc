#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    bool stoneGame(vector<int> &piles)
    {
        int size = piles.size();
        vector<vector<int>> dp(size, vector<int>(size));
        //  len  = 1
        for (int i = 0; i != size; i++)
            dp[i][i] = piles[i];
        for (int i = 1; i != size; i++) // len
        {
            for (int j = 0; j + i < size; j++) // start
            {
                dp[j][i + j] = max(piles[j] - dp[j + 1][i + j], piles[i + j] - dp[j][i + j - 1]);
            }
        }
        return dp[0][size - 1] > 0;
    }
};

int main()
{
    vector<int> v = {5, 3, 4, 5};
    Solution s;
    s.stoneGame(v);
}