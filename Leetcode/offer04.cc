#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    bool findNumberIn2DArray(vector<vector<int>> &matrix, int target)
    {
        int root_x = 0;
        if (matrix.size() < 1)
        {
            return false;
        }
        int root_y = matrix[0].size() - 1;

        while (true)
        {
            if (root_x >= matrix.size() || root_y < 0)
                break;
            if (matrix[root_x][root_y] < target)
            {
                root_x++;
            }
            else if (matrix[root_x][root_y] > target)
            {
                root_y--;
            }
            else
            {
                return true;
            }
        }
        return false;
    }
};

int main()
{
    vector<int> v1 = {1, 4, 7, 11, 15};
    vector<int> v2 = {2, 5, 8, 12, 19};
    vector<int> v3 = {3, 6, 9, 16, 22};
    vector<int> v4 = {10, 13, 14, 17, 24};
    vector<int> v5 = {18, 21, 23, 26, 30};
    vector<vector<int>> vv = {v1, v2, v3, v4, v5};

    Solution s;
    s.findNumberIn2DArray(vv, 7);
}