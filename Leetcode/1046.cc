#include <iostream>
#include <queue>
#include <vector>
using namespace std;

class Solution
{
public:
    int lastStoneWeight(vector<int> &stones)
    {
        priority_queue<int> pq;
        for (auto stone : stones)
        {
            pq.push(stone);
        }
        while (pq.size() > 1)
        {
            int t1 = pq.top();
            pq.pop();
            int t2 = pq.top();
            pq.pop();
            if (t1 - t2 != 0)
                pq.push(abs(t1 - t2));
        }
        return pq.empty() ? 0 : pq.top();
    }
};

int main()
{
}