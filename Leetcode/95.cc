#include <iostream>
#include <vector>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution
{
public:
    vector<TreeNode *> generateTrees(int n)
    {
        vector<TreeNode *> ret;
        for (int i = 1; i <= n; i++)
        {
            vector<TreeNode *> leftChildTree = Generate(1, i - 1);
            vector<TreeNode *> rightChildTree = Generate(i + 1, n);
            for (int j = 0; j != leftChildTree.size(); j++)
            {
                for (int k = 0; k != rightChildTree.size(); k++)
                {
                    TreeNode *root = new TreeNode(i, leftChildTree[j], rightChildTree[k]);
                    ret.push_back(root);
                }
            }
        }
        return ret;
    }

    vector<TreeNode *> Generate(int begin, int end)
    {
        vector<TreeNode *> ret;
        if (end < begin)
        {
            ret.push_back(nullptr);
            return ret;
        }
        else if (end == begin)
        {
            TreeNode *node = new TreeNode(begin, nullptr, nullptr);
            ret.push_back(node);
            return ret;
        }

        for (int i = begin; i <= end; i++)
        {
            vector<TreeNode *> leftChildTree = Generate(begin, i - 1);
            vector<TreeNode *> rightChildTree = Generate(i + 1, end);
            for (int j = 0; j != leftChildTree.size(); j++)
            {
                for (int k = 0; k != rightChildTree.size(); k++)
                {
                    TreeNode *root = new TreeNode(i, leftChildTree[j], rightChildTree[k]);
                    ret.push_back(root);
                }
            }
        }
        return ret;
    }
};

int main()
{
    vector<TreeNode *> result;
    Solution s;
    result = s.generateTrees(3);
    system("pause");
}