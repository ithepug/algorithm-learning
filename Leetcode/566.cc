#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    vector<vector<int>> matrixReshape(vector<vector<int>> &nums, int r, int c)
    {
        int rs = nums.size();
        int cs = nums[0].size();
        int cnt = rs * cs;
        if (r * c != cnt)
        {
            return nums;
        }
        vector<vector<int>> ret;
        for (int i = 0; i != r; i++)
        {
            vector<int> *row = new vector<int>(c, 0);
            ret.push_back(*row);
        }
        for (int i = 0; i != cnt; i++)
        {
            ret[i / c][i % c] = nums[i / cs][i % cs];
        }
        return ret;
    }
};