#include <iostream>
#include <vector>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
public:
    bool isValidBST(TreeNode* root) {
        vector<int> ret(3, root->val);
        if(root->left != nullptr){
            vector<int> left = dfs(root->left);
            if(root->val <= left[1] || left[2] != root->left->val)
            {
                ret[2] = ~root->val ;
            }
            ret[0] = left[0];
        }
        if(root->right != nullptr) {
            vector<int> right = dfs(root->right);
            if(root->val >= right[0] || right[2] != root->right->val)
            {
                ret[2] = ~root->val ;
            }
           ret[1] = right[1];
        }
        return ret[2] == root->val;
    }

    vector<int> dfs(TreeNode* root)
    {
        vector<int> ret(3, root->val);
        if(root->left != nullptr){
            vector<int> left = dfs(root->left);
            if(root->val <= left[1] || left[2] != root->left->val)
            {
                ret[2] = ~root->val;
            }
            ret[0] = left[0];
        }
        if(root->right != nullptr) {
            vector<int> right = dfs(root->right);
            if(root->val >= right[0] || right[2] != root->right->val)
            {
                ret[2] = ~root->val;
            }
            ret[1] = right[1];
        }
        return ret;
    }
};

int main()
{
    TreeNode t1(120);
    TreeNode t2(140);
    t1.right = &t2;
    TreeNode t3(130);
    t2.left = &t3;
    TreeNode t4(119);
    TreeNode t5(135);
    t3.left = &t4;
    t3.right = &t5;
    Solution s;
    s.isValidBST(&t1);
}