#include <iostream>
#include <vector>
using namespace std;

class NumArray
{
private:
    vector<int> tree;
    int n;

public:
    NumArray(vector<int> &nums)
    {
        n = nums.size();
        tree.assign(n, 0);
        for (int i = n, j = 0; i < 2 * n; i++, j++)
            tree[i] = nums[j];
        for (int i = n - 1; i > 0; i--)
            tree[i] = tree[i * 2] + tree[i * 2 + 1];
    }

    void update(int index, int val)
    {
        index += n;
        tree[index] = val;
        while (index > 0)
        {
            int left = index;
            int right = index;
            if (index % 2 == 0)
            {
                right = index + 1;
            }
            else
            {
                left = index - 1;
            }
            tree[index / 2] = tree[left] + tree[right];
            index /= 2;
        }
    }

    int sumRange(int left, int right)
    {
        left += n;
        right += n;

        int sum = 0;
        while (left <= right)
        {
            if ((left % 2) == 1)
            {
                sum += tree[left];
                left++;
            }
            if ((right % 2) == 0)
            {
                sum += tree[right];
                right--;
            }
            left >>= 1;
            right >>= 1;
        }
        return sum;
    }
};

int main()
{
    vector<int> v = {1, 3, 5};
    NumArray num(v);
    int ans = num.sumRange(0, 2);
    num.update(1, 2);
    ans = num.sumRange(0, 2);
}