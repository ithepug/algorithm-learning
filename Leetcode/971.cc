#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;
struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution
{
private:
    vector<int> ans;
    int index = 0;

public:
    vector<int> flipMatchVoyage(TreeNode *root, vector<int> &voyage)
    {
        index = 0;
        if (!dfs(root, voyage))
        {
            ans.assign(1, -1);
        }
        return ans;
    }

    bool dfs(TreeNode *node, const vector<int> &voyage)
    {
        if (node != nullptr)
        {
            if (node->val != voyage[index++])
                return false;
            if (node->left == nullptr && node->right == nullptr)
                return true;

            if (node->left != nullptr && node->left->val == voyage[index]) // 不翻转
            {
                return dfs(node->left, voyage) && dfs(node->right, voyage);
            }
            else if (node->right != nullptr && node->right->val == voyage[index]) // 翻转
            {
                if (node->left != nullptr)
                    ans.push_back(node->val);
                return dfs(node->right, voyage) && dfs(node->left, voyage);
            }
            else
            {
                return false;
            }
        }
        return true;
    }
};

int main()
{
    TreeNode n1(2, nullptr, nullptr);
    TreeNode n2(3, nullptr, nullptr);
    TreeNode n3(1, nullptr, &n1);
    vector<int> v = {1, 2};
    Solution s;
    s.flipMatchVoyage(&n3, v);
}