#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int findTargetSumWays(vector<int> &nums, int target)
    {
        int n = nums.size();
        vector<vector<int>> dp(2, vector<int>(2001, 0));
        dp[0][1000 + nums[0]] += 1;
        dp[0][1000 - nums[0]] += 1;
        for (int i = 1; i != n; i++)
        {
            dp[i & 1].assign(2001, 0);
            for (int j = 0; j != 2001; j++)
            {
                if (j - nums[i] >= 0 && j + nums[i] <= 2000)
                {
                    dp[i & 1][j] += dp[(i - 1) & 1][j - nums[i]] + dp[(i - 1) & 1][j + nums[i]];
                }
            }
        }
        return dp[(n - 1) & 1][target + 1000];
    }
};

int main()
{
    vector<int> v = {1, 999};
    int target = 988;
    Solution s;
    int ans = s.findTargetSumWays(v, target);
    cout << ans;
}