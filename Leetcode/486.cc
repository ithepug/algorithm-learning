#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    bool PredictTheWinner(vector<int> &nums)
    {
        int size = nums.size();
        vector<vector<int>> dp(size, vector<int>(size));
        for (int i = 0; i != size; i++)
        {
            dp[i][i] = nums[i];
        }
        for (int i = 1; i != size; i++)
        {
            for (int j = 0; i + j != size; j++)
            {
                dp[j][i + j] = max(nums[j] - dp[j + 1][i + j], nums[j + i] - dp[j][j + i - 1]);
            }
        }
        return dp[0][size - 1] >= 0;
    }
};

int main()
{
}