#include <iostream>
#include <vector>
using namespace std;

class NumMatrix
{
public:
    NumMatrix(vector<vector<int>> &matrix)
    {
        if (matrix.size() > 0)
        {
            int m = matrix.size();
            int n = matrix[0].size();
            m_ = new vector<vector<int>>(m + 1, vector<int>(n + 1, 0));
            for (int i = 1; i <= m; i++)
            {
                for (int j = 1; j <= n; j++)
                {
                    (*m_)[i][j] = matrix[i - 1][j - 1] + (*m_)[i - 1][j] + (*m_)[i][j - 1] - (*m_)[i - 1][j - 1];
                }
            }
        }
    }

    int sumRegion(int row1, int col1, int row2, int col2)
    {
        if (m_ == nullptr)
        {
            return -1;
        }
        return (*m_)[row2 + 1][col2 + 1] - (*m_)[row2 + 1][col1] - (*m_)[row1][col2 + 1] + (*m_)[row1][col1];
    }

private:
    vector<vector<int>> *m_ = nullptr;
};

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix* obj = new NumMatrix(matrix);
 * int param_1 = obj->sumRegion(row1,col1,row2,col2);
 */
int main()
{
    vector<vector<int>> vv;
    vv.push_back(vector<int>({3, 0, 1, 4, 2}));
    vv.push_back(vector<int>({5, 6, 3, 2, 1}));
    vv.push_back(vector<int>({1, 2, 0, 1, 5}));
    vv.push_back(vector<int>({4, 1, 0, 1, 7}));
    vv.push_back(vector<int>({1, 0, 3, 0, 5}));
    NumMatrix n(vv);
    n.sumRegion(2, 1, 4, 3);
}