#include <iostream>
#include <queue>
using namespace std;

class Solution
{
public:
    int movingCount(int m, int n, int k)
    {
        //bool canGo[100 + 5][100 + 5] = {false};
        //bool vis[100 + 5][100 + 5] = {false};
        vector<vector<bool>> vis(m, vector<bool>(n, false));
        vector<vector<bool>> canGo(m, vector<bool>(n, false));
        for (int i = 0; i != m; i++)
        {
            for (int j = 0; j != n; j++)
            {
                if (getDigitSum(i) + getDigitSum(j) <= k)
                    canGo[i][j] = true;
            }
        }

        int ret = 0;
        queue<pair<int, int>> q;
        q.push({0, 0});
        while (!q.empty())
        {
            pair<int, int> node = q.front();
            q.pop();
            if (!vis[node.first][node.second] && canGo[node.first][node.second])
            {
                vis[node.first][node.second] = true;
                ret++;
                if (node.first + 1 < m)
                    q.push({node.first + 1, node.second});
                if (node.second + 1 < n)
                    q.push({node.first, node.second + 1});
            }
        }
        return ret;
    }

    int getDigitSum(const int &x)
    {
        int ans = 0;
        int tmp = x;
        while (tmp != 0)
        {
            ans += tmp % 10;
            tmp /= 10;
        }
        return ans;
    }
};

int main()
{
    Solution s;
    s.movingCount(16, 25, 7);
}