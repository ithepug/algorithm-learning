#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    bool isToeplitzMatrix(vector<vector<int>>& matrix) {
        for(int i=0;i<matrix[0].size();i++)
        {
            int n = min(matrix[0].size() - i, matrix.size());
            for(int j=1;j<n;j++){
                if(matrix[0][i] != matrix[j][i + j]) 
                {
                    return false;
                }
            }
        }
        for(int i=1;i<matrix.size();i++)
        {
            int n = min(matrix.size()-i, matrix[0].size());
            for(int j=1;j<n;j++)
            {
                if(matrix[i][0] != matrix[i + j][j])
                    return false;
            }
        }
        return true;
    }
};