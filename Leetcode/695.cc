#include <iostream>
#include <queue>
#include <vector>
using namespace std;

class Solution
{
private:
    int ans = 0;
    vector<vector<bool>> vis;

public:
    int maxAreaOfIsland(vector<vector<int>> &grid)
    {
        int n = grid.size();
        if (n < 1)
            return 0;
        int m = grid[0].size();
        vis.assign(n, vector<bool>(m, false));
        for (int i = 0; i != n; i++)
            for (int j = 0; j != m; j++)
                bfs(grid, i, j);
        return ans;
    }

    void bfs(const vector<vector<int>> &grid, int x, int y)
    {
        if (!vis[x][y] && grid[x][y])
        {
            int sum = 0;
            queue<pair<int, int>> q;
            q.push({x, y});
            while (!q.empty())
            {
                pair<int, int> node = q.front();
                q.pop();
                if (!vis[node.first][node.second])
                {
                    vis[node.first][node.second] = true;
                    sum++;
                    if (node.first - 1 >= 0 && grid[node.first - 1][node.second])
                    {
                        q.push({node.first - 1, node.second});
                    }
                    if (node.second + 1 < grid[0].size() && grid[node.first][node.second + 1])
                    {
                        q.push({node.first, node.second + 1});
                    }
                    if (node.first + 1 < grid.size() && grid[node.first + 1][node.second])
                    {
                        q.push({node.first + 1, node.second});
                    }
                    if (node.second - 1 >= 0 && grid[node.first][node.second - 1])
                    {
                        q.push({node.first, node.second - 1});
                    }
                }
            }
            ans = max(ans, sum);
        }
    }
};

int main()
{
    vector<vector<int>> vv = {{1, 1, 0, 0, 0},
                              {1, 1, 0, 0, 0},
                              {0, 0, 0, 1, 1},
                              {0, 0, 0, 1, 1}};
    Solution s;
    cout << s.maxAreaOfIsland(vv);
}