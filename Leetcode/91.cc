#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution
{
public:
    int numDecodings(string s)
    {
        int n = s.size();
        if (!isVaild(s.substr(0, 1)))
            return 0;
        int p = 0;
        int q = 1;
        if (n == 1)
            return q;
        int r = 0;
        if (isVaild(s.substr(1, 1)))
        {
            r += 1;
        }
        if (isVaild(s.substr(0, 2)))
        {
            r += 1;
        }
        if (n == 2)
            return r;
        for (int i = 3; i != n + 1; i++)
        {
            p = q;
            q = r;
            r = 0;
            if (isVaild(s.substr(i - 1, 1)))
            {
                r += q;
            }
            if (isVaild(s.substr(i - 2, 2)))
            {
                r += p;
            }
        }
        return r;
    }

    bool isVaild(const string &s)
    {
        if (s[0] == '0')
            return false;
        if (s.size() == 1)
            return true;
        int sum = 0;
        sum += (s[0] - '0') * 10 + (s[1] - '0');
        if (sum > 0 && sum <= 26)
            return true;
        return false;
    }
};

int main()
{
    Solution solu;
    string s;
    while (cin >> s)
    {
        cout << solu.numDecodings(s) << endl;
    }
}