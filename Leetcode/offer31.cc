#include <iostream>
#include <stack>
#include <vector>
using namespace std;

class Solution
{
public:
    bool validateStackSequences(vector<int> &pushed, vector<int> &popped)
    {
        stack<int> instack;
        vector<int> unpush(pushed);
        int unp = 0;
        for (auto x : popped)
        {
            if (!instack.empty() && instack.top() == x)
            {
                instack.pop();
            }
            else
            {
                while (unp < unpush.size() && unpush[unp] != x)
                {
                    instack.push(unpush[unp]);
                    unp++;
                }
                if(unp == unpush.size())
                    return false;
                unp++;
            }
        }
        return true;
    }
};