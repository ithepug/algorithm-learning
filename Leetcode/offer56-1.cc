#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    vector<int> singleNumbers(vector<int> &nums)
    {
        int tmp = 0;
        for (auto n : nums)
            tmp ^= n;
        int low = 1;
        while ((low & tmp) == 0)
        {
            low <<= 1;
        }
        int a = 0;
        int b = 0;
        for(auto n:nums)
        {
            if(n & low)
                a ^= n;
            else
                b ^= n;
        }
        return {a, b};
    }
};