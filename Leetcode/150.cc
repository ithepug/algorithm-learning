#include <iostream>
#include <stack>
#include <vector>
using namespace std;

class Solution
{
public:
    int evalRPN(vector<string> &tokens)
    {
        stack<int> st;
        int x1, x2;
        for (auto token : tokens)
        {
            switch (token[0])
            {
            case '+':
                x1 = st.top();
                st.pop();
                x2 = st.top();
                st.pop();
                st.push(x2 + x1);
                break;
            case '-':
                // 对负数判断
                if (token.size() == 1)
                {
                    x1 = st.top();
                    st.pop();
                    x2 = st.top();
                    st.pop();
                    st.push(x2 - x1);
                }
                else
                {
                    st.push(atoi(token.c_str()));
                }
                break;
            case '*':
                x1 = st.top();
                st.pop();
                x2 = st.top();
                st.pop();
                st.push(x2 * x1);
                break;
            case '/':
                x1 = st.top();
                st.pop();
                x2 = st.top();
                st.pop();
                st.push(x2 / x1);
                break;
            default:
                st.push(atoi(token.c_str()));
                break;
            }
        }
        return st.top();
    }
};

int main()
{
    vector<string> v = {"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"};
    Solution s;
    s.evalRPN(v);
}