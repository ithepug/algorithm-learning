#include <cstring>
#include <iostream>
#include <vector>

using namespace std;

int uniquePaths(int m, int n)
{
    int w[101][101];
    memset(w, 0, sizeof(w));
    w[1][0] = 1;
    for (int i = 1; i <= m; i++)
        for (int j = 1; j <= n; j++)
        {
            w[i][j] = w[i - 1][j] + w[i][j - 1];
        }
    return w[m][n];
}

int uniquePathsWithObstacles(vector<vector<int>> &obstacleGrid)
{
    int m = obstacleGrid.size();
    int n = obstacleGrid[0].size();
    int w[101][101];
    memset(w, 0, sizeof(w));
    w[1][0] = 1;
    for (int i = 1; i <= m; i++)
        for (int j = 1; j <= n; j++)
        {
            if (i != 1 && j != 1)
            {
                cout << !(obstacleGrid[i - 2][j - 1]);
                cout << !(obstacleGrid[i - 1][j - 2]);
                w[i][j] = w[i - 1][j] * ~(obstacleGrid[i - 2][j - 1]) + w[i][j - 1] * ~(obstacleGrid[i - 1][j - 2]);
            }
            else
                w[i][j] = w[i - 1][j] + w[i][j - 1];
        }
    return w[m][n];
}

int main()
{
    vector<vector<int>> vv = {{0, 0, 0},
                              {0, 1, 0},
                              {0, 0, 0}};
    uniquePathsWithObstacles(vv);
}