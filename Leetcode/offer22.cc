class Solution
{
public:
    ListNode *getKthFromEnd(ListNode *head, int k)
    {
        vector<ListNode *> v;
        if (k < 1)
        {
            return nullptr;
        }
        ListNode *tmp = head;
        while (tmp != nullptr)
        {
            v.push_back(tmp);
            tmp = tmp->next;
        }
        if (k > v.size())
            return v[0];
        return v[v.size() - k];
    }
};