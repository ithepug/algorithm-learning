#include <iostream>
#include <vector>
using namespace std;

class Solution
{
private:
    vector<vector<int>> G;
    vector<int> state;
    bool ret = true;

public:
    void dfs(int node)
    {
        state[node] = 1;
        for (auto x : G[node])
        {
            if (state[x] == 0)
            {
                dfs(x);
            }
            else if (state[x] == 1)
            {
                ret = false;
            }
        }
        state[node] = 2;
    }

    bool canFinish(int numCourses, vector<vector<int>> &prerequisites)
    {
        G.assign(numCourses, vector<int>());
        state.assign(numCourses, 0);
        for (auto &x : prerequisites)
        {
            G[x[1]].push_back(x[0]);
        }
        for (int i = 0; i != numCourses && ret; i++)
        {
            if (state[i] != 2)
                dfs(i);
        }
        return ret;
    }
};

int main()
{
}