#include <iostream>
using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
public:
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2)
    {
        ListNode *headR = new ListNode(0);
        ListNode *tmp = headR;
        ListNode *p1 = l1;
        ListNode *p2 = l2;
        while (p1 != nullptr && p2 != nullptr)
        {
            int val;
            if (p1->val > p2->val)
            {
                val = p2->val;
                p2 = p2->next;
            }
            else
            {
                val = p1->val;
                p1 = p1->next;
            }
            ListNode *node = new ListNode(val);
            tmp->next = node;
            tmp = tmp->next;
        }
        while (p1 != nullptr)
        {
            int val = p1->val;
            p1 = p1->next;
            ListNode *node = new ListNode(val);
            tmp->next = node;
            tmp = tmp->next;
        }
        while (p2 != nullptr)
        {
            int val = p2->val;
            p2 = p2->next;
            ListNode *node = new ListNode(val);
            tmp->next = node;
            tmp = tmp->next;
        }
        return headR->next;
    }
};