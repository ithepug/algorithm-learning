#include <iostream>
#include <vector>
using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
public:
    ListNode *reverseList(ListNode *head)
    {
        ListNode *node = head;
        ListNode *lastNode = nullptr;
        while (node != nullptr)
        {
            ListNode *nextNode = node->next;
            node->next = lastNode;
            lastNode = node;
            node = nextNode;
        }
        return lastNode;
    }
};