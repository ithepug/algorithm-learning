#include <iostream>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution
{
private:
    int sum = 0;

public:
    int sumNumbers(TreeNode *root)
    {
        Search(root, 0);
        return sum;
    }

    void Search(TreeNode *node, int count)
    {
        count *= 10;
        count += node->val;
        if (node->left != nullptr)
            Search(node->left, count);
        if (node->right != nullptr)
            Search(node->right, count);
        if (node->left == nullptr && node->right == nullptr)
        {
            sum += count;
        }
    }
};

int main()
{
    TreeNode n1(2);
    TreeNode n2(3);
    TreeNode n3(1, &n1, &n2);
    Solution s;
    s.sumNumbers(&n3);
}