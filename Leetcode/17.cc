#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution
{
    vector<string> KEY;

public:
    vector<string> letterCombinations(string digits)
    {
        vector<string> ret;
        if (digits.size() != 0)
        {
            KEY.push_back("abc");
            KEY.push_back("def");
            KEY.push_back("ghi");
            KEY.push_back("jkl");
            KEY.push_back("mno");
            KEY.push_back("pqrs");
            KEY.push_back("tuv");
            KEY.push_back("wxyz");

            ret = solve(digits, 0);
        }
        return ret;
    }

    vector<string> solve(string digits, int depth)
    {
        vector<string> ret;
        if (depth == digits.size())
        {
            ret.push_back("");
            return ret;
        }
        char k = digits[depth];
        int d = k - '0' - 2;
        int n = KEY[d].size();
        vector<string> v = solve(digits, depth + 1);
        for (int i = 0; i != n; i++)
        {

            for (int j = 0; j != v.size(); j++)
            {
                string s = "";
                s += KEY[d][i];
                s += v[j];
                ret.push_back(s);
            }
        }
        return ret;
    }
};

int main()
{
    Solution s;
    s.letterCombinations("");
}