#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
private:
    vector<vector<int>> G;
    vector<int> state;
    vector<int> ret;
    bool flag = false;

public:
    void dfs(int node)
    {
        state[node] = 1;
        for (auto x : G[node])
        {
            if (flag)
                return;
            if (state[x] == 0)
                dfs(x);
            else if (state[x] == 1)
                flag = true;
        }
        state[node] = 2;
        ret.push_back(node);
    }

    vector<int> findOrder(int numCourses, vector<vector<int>> &prerequisites)
    {
        G.assign(numCourses, vector<int>());
        state.assign(numCourses, 0);
        for (auto &x : prerequisites)
        {
            G[x[1]].push_back(x[0]);
        }
        for (int i = 0; i != numCourses; i++)
        {
            if (state[i] == 0)
            {
                dfs(i);
                if (flag)
                    return vector<int>();
            }
        }
        reverse(ret.begin(), ret.end());
        return ret;
    }
};

int main()
{
    Solution s;
    vector<vector<int>> vv;
    vv.push_back({1, 0});
    s.findOrder(2, vv);
}