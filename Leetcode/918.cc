#include <iostream>
#include <vector>
using namespace std;
class Solution
{
public:
    int maxSubarraySumCircular(vector<int> &A)
    {
        if (A.size() < 2)
            return A[0];
        int sum = 0;
        for (auto x : A)
            sum += x;
        int pre = A[0];
        int themax = A[0];
        for (int i = 1; i != A.size(); i++)
        {
            pre = pre > 0 ? pre + A[i] : A[i];
            themax = max(themax, pre);
        }
        pre = 0;
        int themin = 0;
        for (int i = 1; i != A.size() - 1; i++)
        {
            pre = pre < 0 ? pre + A[i] : A[i];
            themin = min(themin, pre);
        }
        return max(themax, sum - themin);
    }
};

int main()
{
    Solution s;
    vector<int> v = {1, -2, 3, -2};
    s.maxSubarraySumCircular(v);
}