#include <iostream>
#include <stack>
#include <string>
#include <vector>
using namespace std;

class Solution
{
public:
    string removeKdigits(string num, int k)
    {
        string ret;
        int size = num.size();
        int cnt = 0;
        for (int i = 0; i != size - 1; i++)
        {
            if (num[i] <= num[i + 1])
                ret += num[i];
            else
                cnt++;
            if (!(cnt < k))
            {
                ret += num.substr(i + 1, num.size() - i - 1);
                break;
            }
        }
        ret += num[size - 1];

        ret = ret.substr(0, size - k);
        for (cnt = 0; cnt != ret.size() - 1 && ret[cnt] == '0'; cnt++)
            ;
        ret = ret.substr(cnt);
        if (ret.empty())
            ret = "0";
        return ret;
    }

    string removeKdigits2(string num, int k)
    {
        vector<char> s;
        int cnt = 0;
        for (auto ch : num)
        {
            while (!s.empty() && cnt < k && ch < s.back())
            {
                s.pop_back();
                cnt++;
            }
            s.push_back(ch);
        }

        while(cnt < k)
        {
            s.pop_back();
            cnt++;
        }

        if (s.empty())
            return "0";
        cnt = 0;
        string ret;
        while (s[cnt] == '0')
        {
            cnt++;
            if (cnt == s.size())
                return "0";
        }
        for (; cnt != s.size();cnt++)
            ret.push_back(s[cnt]);
        return ret;
    }
};

int main()
{
    string num = "9";
    int k = 1;
    Solution s;
    s.removeKdigits2(num, k);
}