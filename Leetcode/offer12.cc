#include <iostream>
#include <queue>
#include <vector>
using namespace std;

class Solution
{
public:
    bool exist(vector<vector<char>> &board, string word)
    {
        if (board.size() < 1)
        {
            return false;
        }
        vector<vector<bool>> vis(board.size(), vector<bool>(board[0].size(), false));

        for (int i = 0; i != board.size(); i++)
        {
            for (int j = 0; j != board[0].size(); j++)
            {
                int p = 0;
                if (search(board, {i, j}, word, p, vis))
                    return true;
            }
        }
        return false;
    }

    bool search(const vector<vector<char>> &board, const pair<int, int> &node,
                const string &word, int &p, vector<vector<bool>> &vis)
    {
        bool ret;
        if (vis[node.first][node.second])
            return false;
        vis[node.first][node.second] = true;
        if (board[node.first][node.second] == word[p])
        {
            p++;
            if (p == word.size())
                return true;
            ret = ((node.first + 1 < board.size()) &&
                   (search(board, {node.first + 1, node.second}, word, p, vis))) ||
                  ((node.second + 1 < board[0].size()) &&
                   (search(board, {node.first, node.second + 1}, word, p, vis))) ||
                  ((node.first - 1 >= 0) &&
                   (search(board, {node.first - 1, node.second}, word, p, vis))) ||
                  ((node.second - 1 >= 0) &&
                   (search(board, {node.first, node.second - 1}, word, p, vis)));
            p--;
        }
        else
        {
            ret = false;
        }
        vis[node.first][node.second] = false;
        return ret;
    }
};

int main()
{
    Solution s;
    vector<vector<char>> vv = {{'C', 'A', 'A'},
                               {'A', 'A', 'A'},
                               {'B', 'C', 'D'}};
    s.exist(vv, "AAB");
}