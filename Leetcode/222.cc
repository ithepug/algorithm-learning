#include <iostream>
#include <queue>
#include <stack>

using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x, TreeNode *l, TreeNode *r) : val(x), left(l), right(r) {}
};

class Solution
{
public:
    int countNodes(TreeNode *root)
    {
        int deep = 0;
        TreeNode *node = root;

        if (root == nullptr)
            return 0;

        while (node->left != nullptr)
        {
            deep++;
            node = node->left;
        }

        if (deep == 0)
            return 1;

        int min = 1 << deep;
        int max = (1 << (deep + 1));
        int mid = (max + min) / 2;
        while (min < max)
        {
            if (exist(root, deep, mid))
                min = mid+1;
            else
                max = mid;
            mid = (max + min) >> 1;
        }
        return --min;
    }

    bool exist(TreeNode *root, int deep, int mid)
    {
        int tmp_mid = mid;
        TreeNode *node = root;
        stack<int> s;
        while (tmp_mid > 1)
        {
            s.push(tmp_mid % 2);
            tmp_mid /= 2;
        }
        while (!s.empty())
        {
            int flag = s.top();
            s.pop();
            if (flag)
                node = node->right;
            else
                node = node->left;
        }
        return node != nullptr;
    }
};

int main()
{
    TreeNode tree1(3, NULL, NULL);
    TreeNode tree11(4, NULL, NULL);
    TreeNode tree12(5, NULL, NULL);
    TreeNode tree2(2, &tree11, &tree12);
    TreeNode tree3(1, &tree2, &tree1);
    Solution s;
    s.countNodes(&tree3);
}