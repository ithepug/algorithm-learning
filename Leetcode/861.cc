#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int matrixScore(vector<vector<int>> &A)
    {
        int m = A.size();
        if (m < 1)
            return 0;
        int n = A[0].size();

        int ret = 0;
        for (int i = 0; i != m; i++) // 行
            ret += 1 << (n - 1);

        for (int i = 1; i != n; i++) // 列
        {
            int cnt = 0;
            for (int j = 0; j != m; j++) // 行
            {
                if (A[j][i] == 1)
                {
                    if (A[j][0] == 1)
                        cnt++;
                }
                else
                {
                    if (A[j][0] == 0)
                        cnt++;
                }
            }
            if (cnt < (int)((float)m / 2+0.5))
                cnt = m - cnt;
            ret += cnt * (1 << (n - i - 1));
        }
        return ret;
    }
};

int main()
{
    vector<vector<int>> A = {{0, 0, 1, 1}, {1, 0, 1, 0}, {1, 1, 0, 0}};
    Solution s;
    s.matrixScore(A);
}