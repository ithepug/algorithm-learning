#include <iostream>
#include <vector>
using namespace std;

class CustomFunction
{
public:
    // Returns f(x, y) for any given positive integers x and y.
    // Note that f(x, y) is increasing with respect to both x and y.
    // i.e. f(x, y) < f(x + 1, y), f(x, y) < f(x, y + 1)
    int f(int x, int y) { return x + y; }
};

class Solution
{
public:
    vector<vector<int>> findSolution(CustomFunction &customfunction, int z)
    {
        vector<vector<int>> ret;
        for (int x = 1; x <= 1000; x++)
        {
            // for (int y = 1; y <= 1000; y++)
            // {
            //     if (customfunction.f(x, y) == z)
            //     {
            //         vector<int> v = {x, y};
            //         ret.push_back(v);
            //     }
            //     else if (customfunction.f(x, y) > z)
            //     {
            //         break;
            //     }
            // }
            int lo = 1;
            int hi = 1000 + 1;
            while (lo < hi)
            {
                int mi = lo + ((hi - lo) >> 1);
                if (customfunction.f(x, mi) > z)
                {
                    hi = mi;
                }
                else
                {
                    lo = mi + 1;
                }
            }
            --lo;
            if (lo >= 1 && customfunction.f(x, lo) == z)
            {
                vector<int> v = {x, lo};
                ret.push_back(v);
            }
        }
        return ret;
    }
};

int main()
{
    Solution s;
    CustomFunction c;
    s.findSolution(c, 5);
}