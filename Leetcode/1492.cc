#include <iostream>
using namespace std;

class Solution
{
public:
    int kthFactor(int n, int k)
    {
        int cnt = 0;
        int i;
        for (i = 1; i * i <= n; i++)
        {
            if (n % i == 0)
            {
                cnt++;
                if (cnt == k)
                    return i;
            }
        }
        i--;
        if (i == n / i)
            i--;
        for (; i >= 1; i--)
        {
            if (n % i == 0)
            {
                cnt++;
                if (cnt == k)
                    return n / i;
            }
        }
        return -1;
    }
};