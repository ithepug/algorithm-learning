#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int Left(vector<int> &nums, int lo, int hi, const int &target)
    {
        while (lo < hi)
        {
            int mi = (hi + lo) >> 1;
            if (target == nums[mi])
                hi = mi;
            else if (target != nums[mi + 1])
                lo = mi + 1;
            else
                return mi + 1;
        }
        return 0;
    }

    int Right(vector<int> &nums, int lo, int hi, const int &target)
    {
        while (lo < hi)
        {
            int mi = (hi + lo) >> 1;
            if (target == nums[mi])
                lo = mi + 1;
            else if (target != nums[mi - 1])
                hi = mi;
            else
                return mi - 1;
        }
        return nums.size() - 1;
    }

    int M(vector<int> &nums, int lo, int hi, const int &target)
    {
        while (lo < hi)
        {
            int mi = (hi + lo) >> 1;
            if (target < nums[mi])
                hi = mi;
            else if (target > nums[mi])
                lo = mi + 1;
            else
                return mi;
        }
        return -1;
    }

    vector<int> searchRange(vector<int> &nums, int target)
    {
        int l = -1;
        int r = -1;
        int size = nums.size();
        int mid = M(nums, 0, size, target);
        if (mid == -1)
            return vector<int>{-1, -1};

        l = Left(nums, 0, mid + 1, target);
        r = Right(nums, mid + 1, size, target);
        return vector<int>{l, r};
    }
};

int main()
{
    vector<int> v{6, 6, 8};
    Solution s;
    vector<int> re = s.searchRange(v, 6);
    system("pause");
}