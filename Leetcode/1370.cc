#include <iostream>
using namespace std;

class Solution
{
public:
    string sortString(string s)
    {
        int bucket[26] = {0};
        string ret;
        for (auto c : s)
        {
            bucket[c - 'a']++;
        }
        while (ret.size() != s.size())
        {
            for (int i = 0; i != 26; i++)
                if (bucket[i] != 0)
                {
                    bucket[i]--;
                    ret.push_back('a' + i);
                }

            for (int i = 25; i >= 0; i--)
                if (bucket[i] != 0)
                {
                    bucket[i]--;
                    ret.push_back('a' + i);
                }
        }
        return ret;
    }
};