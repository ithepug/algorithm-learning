#include <iostream>
#include <string>
using namespace std;

class Solution
{
public:
    int countSubstrings(string s)
    {
        int size = s.size();
        int ret = size;
        // len = 1
        for (int i = 0; i != size; i++)
        {
            int l = i - 1;
            int r = i + 1;
            while (l >= 0 && r < size && s[l] == s[r])
            {
                ret += 1;
                l--;
                r++;
            }
        }
        // len = 0
        for (int i = 1; i != s.size(); i++)
        {
            int l = i - 1;
            int r = i;
            while (l >= 0 && r < size && s[l] == s[r])
            {
                ret += 1;
                l--;
                r++;
            }
        }
        return ret;
    }
};