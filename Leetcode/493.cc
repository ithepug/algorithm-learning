#include <iostream>
#include <list>
#include <vector>
using namespace std;

class Solution
{
public:
    int CountPairs(vector<int> &nums, int lo, int hi)
    {
        if (hi == lo)
            return 0;

        int mi = (lo + hi) >> 1;
        int c1 = CountPairs(nums, lo, mi);
        int c2 = CountPairs(nums, mi + 1, hi);
        int ret = c1 + c2;

        // O(N)
        int i = lo;
        int j = mi + 1;
        while (i <= mi)
        {
            while (j < hi && nums[i] > 2 * nums[j])
                j++;
            ret += j - (mi + 1);
            i++;
        }

        // Merge
        vector<int> sortv;
        i = lo;
        j = mi + 1;
        while (i <= mi || j <= hi)
        {
            if ((i <= mi) && (nums[i] < nums[j] || !(j <= hi)))
                sortv.push_back(nums[i]);
            if ((j <= hi) && (nums[j] < nums[i] || !(i <= mi)))
                sortv.push_back(nums[j]);
        }
        for (int i = lo; i <= hi; i++)
            nums[i] = sortv[i];
        return ret;
    }

    int ____reversePairs2(vector<int> &nums)
    {
        if (nums.size() == 0)
            return 0;
        return CountPairs(nums, 0, nums.size() - 1);
    }
};

int main()
{
    Solution s;
    vector<int> v{2147483647, 2147483647, 2147483647, 2147483647, 2147483647, 2147483647};
    cout << s.____reversePairs2(v);
    system("pause");
}