#include <iostream>
using namespace std;

struct TreeNode
{
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution
{
private:
    int count = 0;

public:
    TreeNode *convertBST(TreeNode *root)
    {
        Search(root);
        return root;
    }

    void Search(TreeNode *node)
    {
        if (node != nullptr)
        {
            Search(node->right);
            node->val += count;
            count = node->val;
            Search(node->left);
        }
    }
};