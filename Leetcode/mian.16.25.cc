#include <unordered_map>
using namespace std;

struct Node {
    int key;
    int value;
    Node* pre;
    Node* next;
};

class LRUCache {
private:
    Node* head_;
    Node* tail_;
    int size_;
    int capacity_;
    unordered_map<int, Node*> cache_;

public:
    LRUCache(int capacity)
        : head_(new Node),
          tail_(new Node),
          size_(0),
          capacity_(capacity)
    {
        head_->next = tail_;
        tail_->pre = head_;
    }
    
    int get(int key) {
        auto it = cache_.find(key);
        if(it == cache_.end())
        {
            return -1;
        }
        else
        {
            moveToHead((*it).second);
            return (*it).second->value;
        }
    }
    
    void put(int key, int value) {
        auto it = cache_.find(key);
        if(it == cache_.end())
        {
            Node* node = new Node({key, value, nullptr, nullptr});
            cache_[key] = node;
            insert(head_, node);
            if(++size_ > capacity_)
            {
                removeTail();
                size_--;
            }
        }
        else
        {
            (*it).second->value = value;
            moveToHead((*it).second);
        }
    }

    void remove(Node* node)
    {
        node->next->pre = node->pre;
        node->pre->next = node->next;
    }

    void insert(Node* nodeF, Node* nodeB)
    {
        nodeF->next->pre = nodeB;
        nodeB->next = nodeF->next;
        nodeB->pre = nodeF;
        nodeF->next = nodeB;
    }

    void moveToHead(Node* node)
    {
        remove(node);
        insert(head_, node);
    }

    void removeTail()
    {
        cache_.erase(tail_->pre->key);
        Node* node = tail_->pre;
        remove(tail_->pre);
        delete node;

    }
};

int main()
{
    int result;
    LRUCache lru(2);
    lru.put(1, 1);
    lru.put(2, 2);
    result = lru.get(2);
    lru.put(3, 3);
    result = lru.get(2);
}