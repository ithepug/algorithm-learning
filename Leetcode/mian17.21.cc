#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int trap(vector<int> &height)
    {
        int n = height.size();
        if (n < 1)
            return 0;
        vector<int> l(height);
        vector<int> r(height);
        for (int i = 1; i != n; i++)
        {
            if (l[i] < l[i - 1])
            {
                l[i] = l[i - 1];
            }
        }
        for (int i = n - 1; i != 0; i--)
        {
            if (r[i - 1] < r[i])
            {
                r[i - 1] = r[i];
            }
        }
        int ans = 0;
        for (int i = 0; i != n; i++)
        {
            l[i] = min(l[i], r[i]);
            ans += l[i] - height[i];
        }
        return ans;
    }
};