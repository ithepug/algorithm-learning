#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int minimumTotal(vector<vector<int>> &triangle)
    {
        int *v = new int[triangle[triangle.size() - 1].size()];
        v[0] = triangle[0][0];
        for (int i = 1; i != triangle.size(); i++)
        {
            for (int j = triangle[i].size() - 1; j >= 0; j--)
            {
                int l = INT_MAX;
                int r = INT_MAX;
                if (i - 1 >= 0 && j - 1 >= 0)
                    l = v[j - 1];

                if (i - 1 >= 0 && j < triangle[i - 1].size())
                    r = v[j];
                v[j] = min(l, r) + triangle[i][j];
            }
        }
        int min = v[0];
        for (int i = 0; i != triangle[triangle.size() - 1].size(); i++)
        {
            if (min > v[i])
                min = v[i];
        }
        return min;

        // for(int i=1;i!=triangle.size();i++)
        // {
        //     for(int j=0;j!=triangle[i].size();j++)
        //     {
        //         int l = INT_MAX;
        //         int r = INT_MAX;
        //         if(i - 1 >= 0 && j -1 >= 0)
        //             l = triangle[i - 1][j -1];

        //         if(i - 1 >= 0 && j < triangle[i - 1].size())
        //             r = triangle[i - 1][j];

        //         triangle[i][j] += l>r ? r : l;
        //     }
        // }
        // int min = triangle[triangle.size() - 1][0];
        // for(int i=0;i!=triangle[triangle.size() - 1].size();i++)
        // {
        //     if(min > triangle[triangle.size() - 1][i])
        //         min = triangle[triangle.size() - 1][i];
        // }
        // return min;
    }
};

int main()
{
    Solution s;
    vector<int> v1 = {2};
    vector<int> v2 = {3, 4};
    vector<int> v3 = {6, 5, 7};
    vector<int> v4 = {4, 1, 8, 3};
    vector<vector<int>> v5;
    v5.push_back(v1);
    v5.push_back(v2);
    v5.push_back(v3);
    v5.push_back(v4);
    s.minimumTotal(v5);
}