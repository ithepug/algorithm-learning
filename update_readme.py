import sys
import requests
import lxml
import os
from bs4 import BeautifulSoup

def GetUrl(num):
    response = requests.get('https://www.udebug.com/UVa/' + num)
    content = BeautifulSoup(response.content.decode("utf-8"), "lxml")
    return content.find('a', attrs={'class': 'problem-statement'}).get('href')


def main():
    global file_name
    out = os.popen("ls algorithm-learning/UVa/", 'r', 1).read()
    UVa_list = out.split('\n')
    out = os.popen("ls algorithm-learning/Leetcode/", 'r', 1).read()
    Leetcode_list = out.split('\n')
    num_last = 0
    cnt = 0
    UVa_problem_list = [["--", "--", "--"]]
    Leetcode_problem_list = [["--", "--", "--"]]
    for filename in UVa_list:
        if filename != '':
            if len(filename.split('.')) == 2:
                file_name = filename.split('.')[0]
                file_type = filename.split('.')[1]
                if file_name != num_last:
                    num_last = file_name
                    UVa_problem_list += [["[" + file_name +
                                          "](" + GetUrl(file_name) + ")", "", ""]]
                    cnt += 1
                if file_type == "md":
                    UVa_problem_list[cnt][1] = "[" + filename + "]" + \
                                               "(UVa/" + filename + ")"
                else:
                    UVa_problem_list[cnt][2] = "[" + filename + "]" + \
                                               "(UVa/" + filename + ")"
    cnt = 0
    for filename in Leetcode_list:
        if filename != '':
            if len(filename.split('.')) == 2:
                file_name = filename.split('.')[0]
                file_type = filename.split('.')[1]
                if file_name != num_last:
                    num_last = file_name
                    Leetcode_problem_list += [["[" + file_name +
                                          "](" + ")", "", ""]]
                    cnt += 1
                if file_type == "md":
                    Leetcode_problem_list[cnt][1] = "[" + filename + "]" + \
                                               "(Leetcode/" + filename + ")"
                else:
                    Leetcode_problem_list[cnt][2] = "[" + filename + "]" + \
                                               "(Leetcode/" + filename + ")"
    readme = open("algorithm-learning/README.md", "w")
    readme.write(
        '# Personal algorithm learning repository\n+ using C++\n+ [UVa](https://onlinejudge.org/) and [leetcode]('
        'https://leetcode.com/)\n## UVa\n\n| problem | text | code |\n')
    for problem in UVa_problem_list:
        readme.write("|{0}|{1}|{2}|\n".format(
            problem[0], problem[1], problem[2]))
        
    readme.write('## Leetcode\n\n| problem | text | code |\n')
    for problem in Leetcode_problem_list:
        readme.write("|{0}|{1}|{2}|\n".format(
            problem[0], problem[1], problem[2]))


if __name__ == '__main__':
    main()
