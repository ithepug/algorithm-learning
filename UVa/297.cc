#include <cstring>
#include <iostream>
using namespace std;

char str[1365];
int sp;

int Ceng(int x)
{
    if (x >= 341)
        return 1;
    else if (x >= 85)
        return 4;
    else if (x >= 21)
        return 16;
    else if (x >= 5)
        return 64;
    else if (x >= 1)
        return 256;
    else
        return 1024;
}

int bfs(int *a, int *b)
{
    int i;
    int ans = 0;
    int n = 1365;
    for (i = 0; i != n; i++)
    {
        if (a[i] == 1)
        {
            ans += Ceng(i);
            if (Ceng(i) > 1)
            {
                int t = 4 * i + 1;
                for (int j = t; j != t + 4; j++)
                    b[j] = 0;
            }
        }
        else if (b[i] == 1)
        {
            ans += Ceng(i);
            if (Ceng(i) > 1)
            {
                int t = 4 * i + 1;
                for (int j = t; j != t + 4; j++)
                    a[j] = 0;
            }
        }
    }
    return ans;
}

void Create(int tp, int *t)
{
    for (int i = 0; i != 4 && str[sp] != 0; i++)
    {
        if (str[sp] == 'p')
        {
            sp++;
            Create(4 * tp + 1, t);
            tp++;
        }
        else if (str[sp] == 'f')
        {
            t[tp] = 1;
            sp++;
            tp++;
        }
        else
        {
            sp++;
            tp++;
        }
    }
}

int main()
{
    int n;
    cin >> n;
    //FILE *fp = fopen("297.txt", "w");
    while (n--)
    {
        int ta[1365] = {0};
        int tb[1365] = {0};
        sp = 0;
        cin >> str;
        Create(0, ta);
        sp = 0;
        cin >> str;
        Create(0, tb);
        cout << "There are " << bfs(ta, tb) << " black pixels.\n";
        //fprintf(fp, "There are %d black pixels.\n", bfs(ta, tb));
    }
    //fclose(fp);
}