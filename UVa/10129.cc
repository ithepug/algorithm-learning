#include <cstring>
#include <iostream>
using namespace std;

int vis[26];
int G[26][26];
int in[26];
int out[26];

void dfs(int u)
{
    vis[u] = 1;
    for (int v = 0; v != 26; v++)
        if (!vis[v] && (G[u][v] || G[v][u])) //忽略方向
        {
            dfs(v);
        }
}

bool isConnected(int r)
{
    dfs(r);
    for (int i = 0; i != 26; i++)
    {
        if (!vis[i])
            return false;
    }
    return true;
}

bool isEuler()
{
    bool flag1 = true, flag2 = true;
    for (int i = 0; i != 26; i++)
    {
        if (in[i] != out[i])
        {
            if (flag1 && in[i] - out[i] == 1)
            {
                flag1 = false;
            }
            else if (flag2 && out[i] - in[i] == 1)
            {
                flag2 = false;
            }
            else
                return false;
        }
    }
    return true;
}

int main()
{
    int T, n;
    char s[1001];
    //FILE *fp = fopen("10129.txt", "w");
    cin >> T;
    while (T--)
    {

        int h;
        int t;
        cin >> n;
        memset(vis, 1, sizeof(vis));
        memset(G, 0, sizeof(G));
        memset(in, 0, sizeof(in));
        memset(out, 0, sizeof(out));
        for (int i = 0; i != n; i++)
        {
            cin >> s;
            int len = strlen(s);
            h = s[0] - 'a';
            t = s[len - 1] - 'a';
            G[h][t] = 1;
            in[t]++;
            out[h]++;
            vis[h] = vis[t] = 0;
        }
        if (isConnected(t) && isEuler())
        {
            cout << "Ordering is possible." << endl;
            //fprintf(fp, "Ordering is possible.\n");
        }
        else
        {
            cout << "The door cannot be opened." << endl;
            //fprintf(fp, "The door cannot be opened.\n");
        }
    }
    //fclose(fp);
}