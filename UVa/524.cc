#include <cstring>
#include <iostream>
using namespace std;

int n;
int A[20];
int vis[20];
int isP[40] = {0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1};

//FILE *fp = fopen("524.txt", "w");

void dfs(int cur)
{
    if (cur == n + 1 && isP[A[n] + A[0]])
    {
        for (int i = 0; i != n; i++)
        {
            cout << A[i] << " ";
            //fprintf(fp, "%d ", A[i]);
        }
        cout << A[n] << endl;
        //fprintf(fp, "%d\n", A[n]);
    }
    else
    {
        for (int i = 2; i != n + 2; i++)
            if (!vis[i] && isP[A[cur - 1] + i])
            {
                vis[i] = 1;
                A[cur] = i;
                dfs(cur + 1);
                vis[i] = 0;
            }
    }
}

int main()
{
    int cnt = 1;
    while (cin >> n, n--)
    {
        if(cnt!=1)
        {
            cout << "\n";
            //fprintf(fp, "\n");
        }
        memset(vis, 0, sizeof(vis));
        cout << "Case " << cnt++ << ":" << endl;
        //fprintf(fp, "Case %d:\n", cnt - 1);
        vis[1] = 1;
        A[0] = 1;
        dfs(1);
    }
    //fclose(fp);
}