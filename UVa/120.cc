#include <iostream>
#include <sstream>
#include <string>
using namespace std;

int a[35];
int n;

void swap(int begin)
{
    cout << n - begin-1 << " ";
    int end = 0;
    int temp;
    while (begin > end)
    {
        temp = a[begin];
        a[begin] = a[end];
        a[end] = temp;
        end++;
        begin--;
    }
}

void solve(int begin)
{
    int max = a[begin];
    int maxn = begin;
    for (int i = begin; i != -1; i--)
        if (a[i] > max)
        {
            max = a[i];
            maxn = i;
        }
    if (maxn == 0)
    {
        swap(begin);
    }
    else if (maxn != begin)
    {
        swap(maxn);
        swap(begin);
    }
}

int main()
{
    string s;
    while (getline(cin, s))
    {
        stringstream ss(s);
        n = 0;
        while (ss >> a[n++])
            ;
        for (int i = n - 2; i != 0; i--)
        {
            solve(i);
        }
        cout << "0\n";
    }
}