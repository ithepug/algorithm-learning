#include <iostream>
#include <vector>
using namespace std;

bool flag;
int wl, dl, wr, dr;

struct Node
{
    int w;
    struct Node *l;
    struct Node *r;
    int dl;
    int dr;
    Node(int ww) : w(ww), l(nullptr), r(nullptr), dl(0), dr(0) {}
};

struct Node *Create(int w1, int d1, int w2, int d2)
{
    struct Node *n = new struct Node(0);
    n->dl = d1;
    n->dr = d2;
    if (w1 == 0) //没有叶子的树
    {
        cin >> wl >> dl >> wr >> dr;
        n->l = Create(wl, dl, wr, dr);
    }
    else
    {
        n->l = new struct Node(w1);
    }
    if (w2 == 0) //没有叶子的树
    {
       
        cin >> wl >> dl >> wr >> dr;
        n->r = Create(wl, dl, wr, dr);
    }
    else
    {
        n->r = new struct Node(w2);
    }
    if (1.0 * n->dl / n->r->w != 1.0 * n->dr / n->l->w)
        flag = false;
    n->w += n->l->w + n->r->w;
    return n;
}

int main()
{
    int n;
    cin >> n;
    //FILE *fp = fopen("839.txt", "w");
    while (n--)
    {
        flag = true;
        cin >> wl >> dl >> wr >> dr;
        struct Node *root = Create(wl, dl, wr, dr);
        if (flag)
        {
            //fprintf(fp, "YES\n");
            cout << "YES"
                 << endl;
        }
        else
        {
            //fprintf(fp, "NO\n");
            cout << "NO"
                 << endl;
        }
        if (n)
        {
            cout << endl;
            //fprintf(fp, "\n");
        }
    }
    //fclose(fp);
}