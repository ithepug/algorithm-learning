#include <cstring>
#include <iostream>
#include <queue>
using namespace std;

int d;
int s[3];
int sum, D;
bool vis[205][205];
int dist[205];
//FILE *fp = fopen("10603.txt", "w");

struct Node
{
    int jugs[3];
    int cost;
    Node(int aa, int bb, int cc, int co) : cost(co)
    {
        jugs[0] = aa;
        jugs[1] = bb;
        jugs[2] = cc;
    }
    Node()
    {
        jugs[0] = 0;
        jugs[1] = 0;
        jugs[2] = s[2];
        cost = 0;
    }
    friend bool operator>(struct Node n1, struct Node n2)
    {
        return n1.cost > n2.cost;
    }
};

void update_dist(int r, int v)
{
    if (dist[r] > v || dist[r] < 0)
        dist[r] = v;
}

void bfs(struct Node n)
{
    priority_queue<struct Node, vector<struct Node>, greater<struct Node>> q;
    q.push(n);
    struct Node node;
    while (!q.empty())
    {
        node = q.top();
        q.pop();
        if (node.jugs[0] == d || node.jugs[1] == d || node.jugs[2] == d)
        {
            dist[d] = node.cost;
            return;
        }
        for (int i = 0; i != 3; i++)
            for (int j = 1; j != 3; j++)
            {
                int r = (i + j) % 3;
                int remain = s[r] - node.jugs[r];
                if (node.jugs[i] && remain)
                {
                    if (node.jugs[i] > remain)
                    {
                        struct Node *t = new struct Node(node);
                        t->jugs[i] -= remain;
                        t->jugs[r] = s[r];
                        t->cost += remain;
                        if (!vis[t->jugs[0]][t->jugs[1]])
                        {
                            for (int k = 0; k != 3; k++)
                                update_dist(t->jugs[k], t->cost);
                            q.push(*t);
                            vis[t->jugs[0]][t->jugs[1]] = 1;
                        }
                        delete t;
                    }
                    else
                    {
                        struct Node *t = new struct Node(node);
                        t->jugs[r] += t->jugs[i];
                        t->cost += t->jugs[i];
                        t->jugs[i] = 0;
                        if (!vis[t->jugs[0]][t->jugs[1]])
                        {
                            for (int k = 0; k != 3; k++)
                                update_dist(t->jugs[k], t->cost);
                            q.push(*t);
                            vis[t->jugs[0]][t->jugs[1]] = 1;
                        }
                        delete t;
                    }
                }
            }
    }
    return;
}

void solve()
{
    struct Node *n = new struct Node();
    bfs(*n);
    while (d > 0)
    {
        if (dist[d] != -1)
        {
            cout << dist[d] << " " << d << endl;
            //fprintf(fp, "%d %d\n", dist[d], d);
            return;
        }
        d--;
    }
    cout << "0 0" << endl;
    //fprintf(fp, "0 0\n");
}

int main()
{
    int t;
    cin >> t;
    while (t--)
    {
        cin >> s[0] >> s[1] >> s[2] >> d;
        memset(vis, 0, sizeof(vis));
        memset(dist, -1, sizeof(dist));
        vis[0][0] = 1;
        update_dist(s[2], 0);
        solve();
    }
    //fclose(fp);
}