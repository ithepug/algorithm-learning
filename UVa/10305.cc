#include <iostream>
#include <vector>
using namespace std;
const int maxn = 105;
int main()
{
    int n, m;
    //FILE *fp = fopen("10305.txt", "w");
    while (cin >> n >> m && n)
    {
        int inDegree[maxn] = {0};
        vector<int> after[maxn];
        vector<int> ans;
        int i, j;
        for (int k = 0; k != m; k++)
        {
            cin >> i >> j;
            after[i].push_back(j);
            inDegree[j]++;
        }
        while (true)
        {
            int i;
            for (i = 1; i != n + 1; i++)
            {
                if (inDegree[i] == 0)
                    break;
            }
            if (i == n + 1)
                break;
            ans.push_back(i);
            inDegree[i]--;
            for (int j = 0; j != after[i].size(); j++)
                inDegree[after[i][j]]--;
        }
        for (int i = 0; i != ans.size(); i++)
        {
            cout << ans[i];
            //fprintf(fp, "%d", ans[i]);
            if (i != ans.size() - 1)
            {
                cout << " ";
                //fprintf(fp, " ");
            }
            
        }
        cout << "\n";
        //fprintf(fp, "\n");
    }
    //fclose(fp);
}