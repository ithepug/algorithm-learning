#include <iostream>
#include <stack>
using namespace std;

struct Matrix
{
    int row;
    int col;
} m[26];

int main()
{
    int n;
    cin >> n;
    for (int i = 0; i != n; i++) //录数据
    {
        char rank;
        cin >> rank;
        int r = rank-'A';
        cin >> m[r].row >> m[r].col;
    }
    string expr;
    stack<Matrix> s;
    while (cin >> expr) //解析表达式
    {
        int len = expr.length();
        bool error = false;
        int ans = 0;
        for (int i = 0; i != len; i++)
        {
            if (isalpha(expr[i]))
                s.push(m[expr[i] - 'A']);
            else if (expr[i] == ')')
            {
                Matrix a = s.top();
                s.pop();
                Matrix b = s.top();
                s.pop();
                if (a.row != b.col)
                {
                    error = true;
                    break;
                }
                ans += a.col * b.col*b.row;
                a.row = b.row;
                s.push(a);
            }
        }
        if(error)
            cout << "error" << endl;
        else
            cout << ans << endl;
    }
}