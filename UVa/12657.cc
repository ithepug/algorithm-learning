#include <cmath>
#include <iostream>
using namespace std;

typedef struct box
{
    int last;
    int next;
} BOX;

int main()
{
    int n, m;
    int cnt = 1;
    //FILE *fp = fopen("12657.txt", "w");
    while (cin >> n >> m)
    {
        if (n == -1)
            break;
        bool reverse = false;
        BOX b[100002];
        int command, op1, op2;
        b[0].next = 1;
        for (int i = 1; i != n + 1; i++)
        {
            b[i].last = i - 1;
            b[i].next = i + 1;
        }
        while (m--)
        {
            cin >> command;
            if (command == 4) //reverse
            {
                reverse = !reverse;
            }
            else
            {
                cin >> op1 >> op2;
                if (command == 3) //swap
                {
                    if (b[op2].next == op1)
                    {
                        int tmp = op2;
                        op2 = op1;
                        op1 = tmp;
                    }
                    int tmplast, tmpnext;
                    tmplast = b[op2].last;
                    tmpnext = b[op2].next;
                    b[b[op1].next].last = op2;
                    b[b[op1].last].next = op2;
                    b[b[op2].next].last = op1;
                    b[b[op2].last].next = op1;

                    b[op2].last = b[op1].last ;
                    b[op2].next = b[op1].next == op2 ? op1 : b[op1].next;
                    b[op1].last = tmplast == op1 ? op2 : tmplast;
                    b[op1].next = tmpnext;
                }
                else
                {
                    command--;
                    if (reverse)
                        command = !command;
                    if (command == 1) //x move to y right
                    {
                        if (b[op2].next != op1)
                        {
                            b[b[op1].last].next = b[op1].next;
                            b[b[op1].next].last = b[op1].last;
                            b[op1].last = op2;
                            b[op1].next = b[op2].next;
                            b[b[op2].next].last = op1;
                            b[op2].next = op1;
                        }
                    }
                    else if (b[op2].last != op1) //x move to y left
                    {
                        b[b[op1].last].next = b[op1].next;
                        b[b[op1].next].last = b[op1].last;
                        b[op1].next = op2;
                        b[op1].last = b[op2].last;
                        b[b[op2].last].next = op1;
                        b[op2].last = op1;
                    }
                }
            }
        }
        uint64_t ans = 0;
        int r;
        if (n % 2 == 0 && reverse)
            r = b[0].next;
        else
            r = 0;
        int size;
        size = round(n / 2.0);
        for (int i = 0; i != size; i++)
        {
            ans += b[r].next;
            r = b[b[r].next].next;
        }
        cout << "Case " << cnt << ": " << ans << endl;
        //fprintf(fp, "Case %d: %lld\n", cnt, ans);
        cnt++;
    }
    //fclose(fp);
}