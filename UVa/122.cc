#include <cstring>
#include <iostream>
#include <queue>
using namespace std;

struct Node
{
    int key;
    Node *left;
    Node *right;
    Node(int k) : key(k), left(nullptr), right(nullptr){};
};

Node *root;

void AddNode(int k, char *s)
{
    int len = strlen(s);
    Node *p = root;
    if (len == 1)
    {
        root->key = k;
        return;
    }
    for (int i = 0; i != len - 1; i++)
    {
        if (s[i] == 'L')
        {
            if (p->left == nullptr)
            {
                p->left = new Node(-1);
            }
            p = p->left;
        }
        else
        {
            if (p->right == nullptr)
            {
                p->right = new Node(-1);
            }
            p = p->right;
        }
    }
    p->key = k;
}

int main()
{
    char str[290];
    while (scanf("%s", str) != EOF)
    {
        root = new Node(-1);
        int k;
        int cnt;
        sscanf(str + 1, "%d", &k);
        AddNode(k, strchr(str, ',') + 1);
        for (cnt = 1; scanf("%s", str) && str[1] != ')'; cnt++)
        {
            sscanf(str + 1, "%d", &k);
            AddNode(k, strchr(str, ',') + 1);
        }
        queue<Node *> q;
        queue<int> ans;
        q.push(root);
        while (!q.empty())
        {
            Node *p = q.front();
            q.pop();
            ans.push(p->key);
            if (p->left != nullptr)
                q.push(p->left);
            if (p->right != nullptr)
                q.push(p->right);
        }
        if (cnt == ans.size())
        {
            for (int i = 0; i != cnt; i++)
            {
                cout << ans.front();
                ans.pop();
                if (i != cnt - 1)
                    printf(" ");
                else
                    printf("\n");
            }
        }
        else
        {
            cout << "not complete" << endl;
        }
    }
}