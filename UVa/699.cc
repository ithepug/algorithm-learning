#include <iostream>
using namespace std;

int *minr, *maxr;

void Create(int *ans, int x)
{
    *ans += x;
    if (ans > maxr)
        maxr = ans;
    if (ans < minr)
        minr = ans;
    int i;
    cin >> i;
    if (i != -1)
    {
        Create(ans - 1, i);
    }
    cin >> i;
    if (i != -1)
    {
        Create(ans + 1, i);
    }
}

int main()
{
    int x;
    int cnt = 1;
    //FILE *fp = fopen("699.txt", "w");
    while (cin >> x && x != -1)
    {
        int ans[1000] = {0};
        minr = &ans[500];
        maxr = &ans[500];
        Create(ans + 500, x);
        cout << "Case " << cnt++ << ":" << endl;
        //fprintf(fp, "Case %d:\n", cnt - 1);
        for (; minr != maxr; minr++)
        {
            cout << *minr << " ";
            //fprintf(fp, "%d ", *minr);
        }
        cout << *maxr << endl
             << endl;
        //fprintf(fp, "%d\n\n", *maxr);
    }
}