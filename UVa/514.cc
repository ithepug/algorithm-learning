#include <iostream>
#include <stack>
#define maxn 1000
using namespace std;
int main()
{
    int n;
    int target[maxn];
    while (cin >> n && n)
    {
        int x;
        while (cin >> x && x)
        {        
            stack<int> s;
            target[1] = x;
            int flag = 1;
            for (int i = 2; i <= n; i++)
            {
                cin >> target[i];
            }
            int origin = 1;
            int rank = 1;
            while (rank <= n)
            {
                if (origin == target[rank])
                {
                    origin++;
                    rank++;
                }
                else if (!s.empty() && target[rank] == s.top())
                {
                    s.pop();
                    rank++;
                }
                else if (origin <= n)
                {
                    s.push(origin++);
                }
                else
                {
                    flag = 0;
                    break;
                }
            }
            cout << (flag ? "Yes" : "No") << endl;
        }
        cout << endl;
    }
}