#include <cstring>
#include <iostream>
using namespace std;

char map[100][101];
int tag[100][100];
int row, col;

void Dfs(int r, int c, int t)
{
    if (r < 0 || c < 0 || r >= row || c >= col)
        return;
    if (tag[r][c] > 0)
        return;
    if (map[r][c] == '@')
    {
        tag[r][c] = t;
        for (int i = -1; i != 2; i++)
            for (int j = -1; j != 2; j++)
            {
                if (i != 0 || j != 0)
                    Dfs(r + i, c + j, t);
            }
    }
}

int main()
{
    //FILE *fp = fopen("572.txt", "w");
    while (cin >> row >> col && row)
    {
        for (int i = 0; i != row; i++)
        {
            cin >> map[i];
        }
        memset(tag, 0, sizeof(tag));
        int cnt = 0;
        for (int i = 0; i != row; i++)
            for (int j = 0; j != col; j++)
            {
                if (map[i][j] == '@' && tag[i][j] == 0)
                    Dfs(i, j, ++cnt);
            }
        cout << cnt << endl;
        //fprintf(fp, "%d\n", cnt);
    }
    //fclose(fp);
}