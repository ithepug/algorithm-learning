#include <iostream>
#include <sstream>
using namespace std;

int inorder[10000], postorder[10000];
int n;
int sum, mins, minv;

struct Node
{
    int value;
    struct Node *left;
    struct Node *right;
    Node(int v, struct Node *l, struct Node *r) : value(v), left(l), right(r) {}
};

struct Node *CreateTree(int l1, int r1, int l2, int r2)
{
    if (l1 > r1)
        return nullptr;
    struct Node *node = new struct Node(postorder[r2], nullptr, nullptr);
    sum += node->value;
    int p = 0;
    while (inorder[p] != postorder[r2])
        p++;
    int lcnt = p - l1;
    node->right = CreateTree(p + 1, r1, l2 + lcnt, r2 - 1);
    node->left = CreateTree(l1, p - 1, l2, l2 + lcnt - 1);
    if (node->right == nullptr && node->left == nullptr)
    {
        if (sum < mins)
        {
            minv = node->value;
            mins = sum;
        }
    }
    sum -= node->value;
    return node;
}

bool Read(int *order)
{
    string s;
    int x;
    n = 0;
    getline(cin, s);
    stringstream ss(s);
    while (ss >> x)
    {
        order[n++] = x;
    }
    return n; //仅用于判断读入的字符串是否为空
}

int main()
{
    while (Read(inorder))
    {
        Read(postorder);
        sum = 0;
        mins = 1000000000;
        minv = postorder[n - 1];
        CreateTree(0, n - 1, 0, n - 1);
        cout << minv << endl;
    }
}