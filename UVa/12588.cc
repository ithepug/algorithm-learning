#include <algorithm>
#include <cstring>
#include <iostream>
using namespace std;

bool flag;
long long ans[1005];
long long v[1005];
int ban[10];
int k;
uint64_t maxd;

uint64_t get_first(uint64_t a, uint64_t b)
{
    for (int i = 0;; i++)
        if (i * a >= b)
            return i;
}

bool better(int d)
{
    for (int i = d; i >= 0; i--)
        if (ans[i] != v[i])
            return ans[i] == -1 || v[i] < ans[i];
    return false;
}

bool isban(int v)
{
    bool flag = false;
    for (int i = 0; i != k; i++)
        if (v == ban[i])
        {
            flag = true;
            break;
        }
    return flag;
}

bool dfs(uint64_t d, uint64_t from, uint64_t a, uint64_t b)
{
    if (d == maxd)
    {
        if (b % a)
            return false;
        v[d] = b / a;
        if (better(d))
            memcpy(ans, v, sizeof(v[0]) * (d + 1));
        return true;
    }
    bool ok = false;
    from = max(from, get_first(a, b));
    for (int i = from;; i++)
    {
        if (a * i >= b * (maxd - d + 1)) //剪枝
            break;
        if (isban(i))
            continue;
        v[d] = i;
        uint64_t b2 = b * i;
        uint64_t a2 = a * i - b;
        uint64_t g = __gcd(a2, b2);
        if (dfs(d + 1, i + 1, a2 / g, b2 / g))
            ok = true;
    }
    return ok;
}

void solve(int a, int b)
{
    for (maxd = 1;; maxd++)
    {
        memset(ans, -1, sizeof(ans));
        if (dfs(0, get_first(a, b), a, b))
        {
            break;
        }
    }
}

int main()
{
    //FILE *fp = fopen("12588.txt", "w");
    int n;
    int a, b;
    cin >> n;
    for (int j = 0; j != n; j++)
    {
        cin >> a >> b >> k;
        memset(ban, 0, sizeof(ban));
        for (int i = 0; i != k; i++)
        {
            cin >> ban[i];
        }
        solve(a, b);
        cout << "Case " << j + 1 << ": ";
        cout << a << "/" << b << "=";
        //fprintf(fp, "Case %d: %d/%d=", j + 1, a, b);
        int i;
        for (i = 0; ans[i + 1] != -1; i++)
        {
            cout << "1/" << ans[i] << "+";
            //fprintf(fp, "1/%d+", ans[i]);
        }
        cout << "1/" << ans[i] << endl;
        //fprintf(fp, "1/%d\n", ans[i]);
    }
}