# UVa12657
![](./images/12657.png)

本题的题意并不难理解，主要就是对链表移动、交换和倒序操作。在对链表节点的操作时需要非常小心，否则将需要花很长的时间来进行调试。而在对链表的这三种操作中，交换是需要特别注意的。我就在做这题的时候忘记了考虑两个箱子相邻时交换的情况，导致花了相当长的时间来调试。而且两个箱子相邻的情况下又需要分两种考虑，你需要考虑两个箱子之间的顺序，而我索性就将其中的一种转化成另一种进行交换操作。
```c
if (b[op2].next == op1)
{
    int tmp = op2;
    op2 = op1;
    op1 = tmp;
}
```
值得一提的还有对链表倒序的操作上，一个倒序的操作就需要操作每个节点的指针，对于长的链表来说开销也是很大的。于是乎，我们bool类型的变量来标记这一串链表是否反转。

```c
if (command == 4) //reverse
{
    reverse = !reverse;
}
```

由此，我们后面的移动操作和结果累加都需考虑这一串链表是否反转了。

```c
command--;//2 to ture //1 to false
if (reverse)
    command = !command;
```
有偶数个节点且反转的话，从第2个节点开始累加。
```c
if (n % 2 == 0 && reverse)
    r = b[0].next;
else
    r = 0;
```
```c
for (int i = 0; i != size; i++)
{
    ans += b[r].next;
    r = b[b[r].next].next;
}
```