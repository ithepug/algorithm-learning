#include <cstring>
#include <iostream>
using namespace std;

int x, y;
const int N = 8;
int vis[3][20];
int cnt;
int C[8];

void search(int cur)
{
    if (cur == N)
    {
        int i;
        printf("%2d      ", ++cnt);
        for (i = 0; i != N - 1; i++)
            cout << C[i]+1 << " ";
        cout << C[i]+1 << endl;
    }
    else if(cur == y)
    {
        C[cur] = x;
        search(cur + 1);
    }
    else 
    {
        
        for (int i = 0; i != N;i++)
        {
            if(!vis[0][i]&&!vis[1][i+cur]&&!vis[2][cur-i+N])
            {
                C[cur] = i;
                vis[0][i] = vis[1][i + cur] = vis[2][cur - i + N] = 1;
                search(cur + 1);
                vis[0][i] = vis[1][i + cur] = vis[2][cur - i + N] = 0;
            }
        }
    }
}

int main()
{
    int n;
    cin >> n;
    while (n--)
    {
        memset(vis, 0, sizeof(vis));
        cnt = 0;
        cin >> x >> y;
        x--;
        y--;
        cout << "SOLN       COLUMN" << endl;
        cout << " #      1 2 3 4 5 6 7 8\n"
             << endl;
        vis[0][x] = 1;
        vis[1][x + y] = 1;
        vis[2][y - x + N] = 1;
        search(0);
        if(n!=0)
            cout << "\n";
    }
}