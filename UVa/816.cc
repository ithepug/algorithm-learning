#include <cstring>
#include <iostream>
#include <queue>
using namespace std;

//FILE *fp = fopen("816.txt", "w");

struct Node
{
    int y;
    int x;
    int dir;
    Node(int yy, int xx, int d) : y(yy), x(xx), dir(d){};
    Node(){};
} p[10][10][4];

int target_x, target_y;
const char *direction = "NESW";
const char *turn = "FLR";
bool flag;
int d[10][10][4];
int inside[10][10];
int go_ok[10][10][4][3];
const int dy[] = {-1, 0, 1, 0};
const int dx[] = {0, 1, 0, -1};

int GetDir(char x)
{
    return strchr(direction, x) - direction;
}

int GetTurn(char x)
{
    return strchr(turn, x) - turn;
}

void print(Node n)
{
    vector<Node> np;
    Node x(n);
    np.push_back(n);
    while (d[x.y][x.x][x.dir] != 1)
    {
        x = p[x.y][x.x][x.dir];
        np.push_back(x);
    }
    x = p[x.y][x.x][x.dir];
    np.push_back(x);
    int len = np.size();
    for (int i = len - 1, cnt = 0; i != -1; i--)
    {
        if (cnt % 10 == 0)
        {
            printf(" ");
            //fprintf(fp, " ");
        }
        printf(" (%d,%d)", np[i].y, np[i].x);
        //fprintf(fp, " (%d,%d)", np[i].y, np[i].x);
        if (++cnt % 10 == 0)
        {
            printf("\n");
            //fprintf(fp, "\n");
        }
    }
    if (len  % 10 != 0)
    {
        printf("\n");
        //fprintf(fp, "\n");
    }
}

Node Go(Node n, int t)
{
    int dir = n.dir;
    if (t == 1)
        dir = (dir + 3) % 4;
    else if (t == 2)
        dir = (dir + 1) % 4;
    return Node(n.y + dy[dir], n.x + dx[dir], dir);
}

void Solve(Node u)
{
    queue<Node> q;
    q.push(u);
    while (!q.empty())
    {
        Node n = q.front();
        q.pop();
        if (n.y == target_y && n.x == target_x)
        {
            print(n);
            flag = false;
            break;
        }
        for (int i = 0; i != 3; i++)
        {
            Node v = Go(n, i);
            if (inside[v.y][v.x] && go_ok[n.y][n.x][n.dir][i] && d[v.y][v.x][v.dir] <= 0)
            {
                d[v.y][v.x][v.dir] = d[n.y][n.x][n.dir] + 1;
                p[v.y][v.x][v.dir] = n;
                q.push(v);
            }
        }
    }
}

int main()
{
    char name[21];
    int x, y;
    char dir;
    while (cin >> name && strcmp(name, "END"))
    {
        /********Initialization********/
        flag = true;
        memset(go_ok, 0, sizeof(go_ok));
        memset(inside, 0, sizeof(inside));
        memset(d, -1, sizeof(d));
        /********Input Enterance and Goal********/
        cin >> y >> x >> dir >> target_y >> target_x;
        Node u(y, x, GetDir(dir));
        d[u.y][u.x][u.dir] = 0;
        Node uu = Go(u, 0);
        p[uu.y][uu.x][uu.dir] = u;
        d[uu.y][uu.x][uu.dir] = 1;
        inside[target_y][target_x] = 1;
        /********Input Node********/
        while (cin >> y && y != 0)
        {
            cin >> x;
            inside[y][x] = 1;
            char str[5];
            while (cin >> str && str[0] != '*')
            {
                int len = strlen(str);
                for (int i = 1; i != len; i++)
                {
                    go_ok[y][x][GetDir(str[0])][GetTurn(str[i])] = 1;
                }
            }
        }
        cout << name << endl;
        //fprintf(fp, "%s\n", name);
        Solve(uu);
        if (flag)
        {
            printf("  No Solution Possible\n");
            //fprintf(fp, "  No Solution Possible\n");
        }
    }
    //fclose(fp);
}